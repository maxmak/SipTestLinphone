//
//  ViewController.m
//  XQLdsdSip
//
//  Created by ladystyle100 on 2017/8/3.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import "ViewController.h"
#import "XQCallVC.h"
#import "XQPhotosVC.h"
#import "ChatVC.h"

#import "LinphoneManager.h"

@interface ViewController () <LdsdSipManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *registerLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [LdsdSipManager manager].delegate = self;
    
    //[[LdsdSipManager manager] enableLogs:NO];
    [[LdsdSipManager manager] initSipServer];
    
    [self creatButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [LdsdSipManager manager].delegate = self;
}

#pragma mark -- LdsdSipManagerDelegate

- (void)ldsdRegistrationStateUpdate:(LdsdSipRegistration)status username:(NSString *)username message:(NSString *)message {
    switch (status) {
        case LdsdSipRegistrationOk:
            NSLog(@"\n\n\n\n\n\n userName = %@, 登录成功 \n\n\n\n\n\n\n", username);
            self.registerLabel.text = @"登录成功";
            break;
            
        case LdsdSipRegistrationNone:
            self.registerLabel.text = @"未知状态";
            break;
            
        case LdsdSipRegistrationFailed:
            self.registerLabel.text = @"登录失败";
            break;
            
        case LdsdSipRegistrationCleared:
            self.registerLabel.text = @"退出登录成功";
            break;
            
        case LdsdSipRegistrationProgress:
            self.registerLabel.text = @"正在登录/退出登录";
            break;
            
        default:
            break;
    }
}

- (void)ldsdCallStateChangeWithFrom:(NSString *)from to:(NSString *)to fromDisplayName:(NSString *)fromDisplayName status:(LdsdSipCall)status message:(NSString *)message {
    if (status == LdsdSipCallIncomingReceived) {
        XQCallVC *vc = [[XQCallVC alloc] initWithNibName:NSStringFromClass([XQCallVC class]) bundle:nil];
        vc.isOutCall = NO;
        vc.sipNum = from;
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (void)ldsdSendMessageWithFrom:(NSString *)from to:(NSString *)to status:(LdsdSipChatMessageState)status message:(NSString *)message messageID:(NSString *)messageID {
    if (status == LdsdSipChatMessageStateDelivered) {
        NSLog(@"ldsdSipSendMessageWithStatus 发送成功, text = %@", message);
    }else if (status == LdsdSipChatMessageStateNotDelivered) {
        NSLog(@"ldsdSipSendMessageWithStatus 发送失败, text = %@", message);
    }else if (status == LdsdSipChatMessageStateInProgress) {
        NSLog(@"ldsdSipSendMessageWithStatus 正在发送, text = %@", message);
    }
}

- (void)ldsdReceiveMessageWithFrom:(NSString *)from to:(NSString *)to fromDisplayName:(NSString *)fromDisplayName message:(NSString *)message messageID:(NSString *)messageID {
    self.messageLabel.text = message;
}

#pragma mark -- 呼叫
- (void)callOut {
    XQCallVC *vc = [[XQCallVC alloc] initWithNibName:NSStringFromClass([XQCallVC class]) bundle:nil];
    vc.sipNum = OpponentSip_;
    vc.isOutCall = YES;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark -- 开门
- (void)openDoor {
    // 地址字符串: sip:username@domain
    // const char *identity = "sip:075500010100010100006@semtec-test.weedoor.com";
//    NSString *sipaddress = [SipCompress compressWithSip:selfSip_ Mobile:@"18000000000" Days:0];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"YYYY.MM.DD HH:mm:ss";
    NSString *message = [NSString stringWithFormat:@"当前时间 %@", [format stringFromDate:date]];
    
    NSString *messageID = [[LdsdSipManager manager] ldsdSendMessage:OpponentSip_ message:message];
    NSLog(@"messageID = %@", messageID);
}

#pragma mark -- 登录
- (void)login {
//    [[LdsdSipManager manager] ldsdLogin:@"002100010300010150111" password:@"X8MR8Rxa" displayName:@"2222" domain:@"semtec-test.weedoor.com" port:@"9647" withTransport:@"udp"];
    
    // recom-s2.ldsd.cc
//    [[LdsdSipManager manager] ldsdLogin:@"001000270300010113181" password:@"OPAOqgHe" displayName:@"Test1318" domain:@"recom-s2.ldsd.cc" port:@"9647" withTransport:@"tcp"];
    
    // md-ss1.weedoor.com
//    [[LdsdSipManager manager] ldsdLogin:@"075500070300010101011" password:@"Wr6JrAOA" displayName:@"mdss1" domain:@"md-ss1.weedoor.com" port:@"9647" withTransport:@"tcp"];

//    [[LdsdSipManager manager] ldsdLogin:@"15000000000" password:@"992f5df6" displayName:@"mdss1" domain:@"md-ss1.weedoor.com" port:@"9647" withTransport:@"udp"];
    
    // yuexiu-ss1.weedoor.com
    
    
    /*
    // 2 ~ 5
    for (int i = 0; i < 4; i++) {
        NSString *sip = [NSString stringWithFormat:@"44010%d", i + 2];
//        [[LdsdSipManager manager] ldsdLogin:sip password:selfPassword_ displayName:displayname_ domain:domain_ port:port_ withTransport:transport_];
        [[LdsdSipManager manager] ldsdLogin:sip password:selfPassword_ displayName:displayname_ domain:@"120.24.209.135:5060;" port:port_ withTransport:transport_];
    }
    */
    
    
    BOOL is = [[LdsdSipManager manager] ldsdLogin:selfSip_ password:selfPassword_ displayName:displayname_ domain:domain_ port:port_ withTransport:transport_];
    
    if (is) {
        //[[LdsdSipManager manager] ldsdSetStun:@"stunserver.org"];
        //[[LdsdSipManager manager] ldsdStunEnabled:YES];
        //[[LdsdSipManager manager] ldsdICEEnabled:YES];
    }
}

- (void)viewAllAddress {
    const bctbx_list_t * pCfgList = linphone_core_get_proxy_config_list(LC);
    if (pCfgList) {
        LinphoneProxyConfig *pCfg = pCfgList->data;
        
        const LinphoneAddress *adr = linphone_proxy_config_get_identity_address(pCfg);
        if (adr) {
            NSLog(@"userName = %@", [XQLPOCBridge getUserNameWithAddress:adr]);
        }
        
        pCfgList = pCfgList->next;
    }
}

#pragma mark -- CreateBtn

- (void)creatButton {
    NSArray *arr = @[@"登录", @"退出登录", @"拨打电话", @"开门", @"查看快照", @"XQChat", @"查看本地userName"];
    CGFloat width = 150;
    CGFloat height = 44;
    CGFloat topSpacing = 110;
    CGFloat spacing = 20;
    
    for (int i = 0; i < arr.count; i++) {
        UIButton *btn = [self buttonWithRect:CGRectMake((self.view.bounds.size.width - width) / 2, topSpacing + (height + spacing) * i, width, height) title:arr[i] tag:100 + i];
        
        [self.view addSubview:btn];
    }
}

- (UIButton *)buttonWithRect:(CGRect)rect title:(NSString *)title tag:(NSInteger)tag {
    UIButton *btn = [[UIButton alloc] initWithFrame:rect];
    btn.backgroundColor = [UIColor orangeColor];
    btn.tag = tag;
    [btn setTitle:title forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(respondsToButtn:) forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}

- (void)respondsToButtn:(UIButton *)sender {
    switch (sender.tag) {
        case 100:{
            [self login];
        }
            break;
            
        case 101:{
            //[[LdsdSipManager manager] ldsdLogout:selfSip_ domain:domain_];
            [[LdsdSipManager manager] ldsdLogoutAll];
        }
            break;
            
        case 102:{
            [self callOut];
        }
            break;
            
        case 103:{
            [self openDoor];
        }
            break;
            
        case 104:{
            XQPhotosVC *vc = [XQPhotosVC new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 105:{
            ChatVC *vc = [ChatVC new];
            vc.sipNum = OpponentSip_;
            vc.selfSipNum = selfSip_;
            vc.selfDisplayName = displayname_;
            vc.domain = domain_;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 106: {
            [self viewAllAddress];
        }
            break;
            
        default:
            break;
    }
}



@end













