//
//  XQCallVC.h
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/3.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XQCallVC : UIViewController

/** YES呼出 */
@property (nonatomic, assign) BOOL isOutCall;
@property (nonatomic, copy) NSString *sipNum;

@end
