//
//  XQCallVC.m
//  XQLdsdSip
//
//  Created by ladystyle100 on 2017/8/3.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import "XQCallVC.h"
#import "LinphoneManager.h"

@interface XQCallVC () <LdsdSipManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIView *rightVideoView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *muteBtn;
@property (weak, nonatomic) IBOutlet UIButton *answerBtn;

@end

@implementation XQCallVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.videoView.hidden = YES;
    self.rightVideoView.hidden = YES;
    
    [LdsdSipManager manager].delegate = self;
    
    // 设置视频
    [self setVideo];
    
    if (self.isOutCall) {
        // 075500010100010100006
        [[LdsdSipManager manager] ldsdCall:self.sipNum] ? 1 : [self dismissViewControllerAnimated:YES completion:nil];
        self.answerBtn.hidden = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[LdsdSipManager manager] ldsdSetRemoteVideoView:nil];
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setVideo {
    [[LdsdSipManager manager] ldsdSetRemoteVideoView:self.videoView];
}

#pragma mark -- LdsdSipManagerDelegate

- (void)ldsdCallStateChangeWithFrom:(NSString *)from to:(NSString *)to fromDisplayName:(NSString *)fromDisplayName status:(LdsdSipCall)status message:(NSString *)message {
    switch (status) {
        case LdsdSipCallOutgoingInit:
            self.statusLabel.text = @"呼出电话初始化中";
            break;
            
        case LdsdSipCallOutgoingProgress:
            self.statusLabel.text = @"拨号中";
            break;
            
        case LdsdSipCallOutgoingRinging:
            self.statusLabel.text = @"对方正在响铃";
            break;
            
        case LdsdSipCallConnected:
            self.statusLabel.text = @"通话已连接";
            self.answerBtn.hidden = YES;
            break;
            
        case LdsdSipCallStreamsRunning:{
            self.statusLabel.text = @"流媒体已建立";
        }
            break;
            
        case LdsdSipCallError:
            self.statusLabel.text = @"通话错误";
            [self dismiss];
            break;
            
        case LdsdSipCallEnd:
            self.statusLabel.text = @"通话结束";
            [self dismiss];
            NSLog(@"\n\n\n\n\n通话结束\n\n\n\n\n");
            break;
            
        case LdsdSipCallEarlyUpdatedByRemote:
            self.statusLabel.text = @"对方未应答通话";
            [self dismiss];
            break;
            
        case LdsdSipCallEarlyUpdating:
            self.statusLabel.text = @"我未应答对方通话";
            [self dismiss];
            break;
            
        case LdsdSipCallReleased:
            self.statusLabel.text = @"通话被释放";
            [self dismiss];
            NSLog(@"\n\n\n\n\n通话被释放\n\n\n\n\n");
            break;
            
        case LdsdSipCallUpdating:
            NSLog(@"通话状态更新");
            break;
            
        default:
            break;
    }
}

- (void)ldsdCallStateUpdateWithFrom:(NSString *)from to:(NSString *)to fromDisplayName:(NSString *)fromDisplayName type:(LdsdSipStreamType)type {
    if (type == LdsdSipStreamTypeVideo) {
        self.videoView.hidden = NO;
    }
}

- (void)ldsdSendMessageWithFrom:(NSString *)from to:(NSString *)to status:(LdsdSipChatMessageState)status message:(NSString *)message messageID:(NSString *)messageID {
    if (status == LdsdSipChatMessageStateDelivered) {
        NSLog(@"ldsdSipSendMessageWithStatus 发送成功, text = %@", message);
    }else if (status == LdsdSipChatMessageStateNotDelivered) {
        NSLog(@"ldsdSipSendMessageWithStatus 发送失败, text = %@", message);
    }else if (status == LdsdSipChatMessageStateInProgress) {
        NSLog(@"ldsdSipSendMessageWithStatus 正在发送, text = %@", message);
    }
}

- (void)ldsdReceiveMessageWithFrom:(NSString *)from to:(NSString *)to fromDisplayName:(NSString *)fromDisplayName message:(NSString *)message messageID:(NSString *)messageID {
    NSLog(@"ldsdSipReceiveMessageWithFrom from = %@, text = %@, displayName = %@", from, message, fromDisplayName);
}

#pragma mark -- respondsTo..

- (IBAction)respondsToMute:(UIButton *)sender {
    // 话筒 or 静音
    sender.selected = !sender.selected;
    [[LdsdSipManager manager] ldsdMuteEnabled:sender.selected];
}

- (IBAction)respondsToHangup:(UIButton *)sender {
    [[LdsdSipManager manager] ldsdHangUp];
}

- (IBAction)respondsToAnswer:(UIButton *)sender {
    [[LdsdSipManager manager] ldsdAnswer];
}

- (IBAction)respondsToOD:(id)sender {
    NSDate *date = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"YYYY.MM.dd HH:mm:ss";
    NSString *message = [NSString stringWithFormat:@"当前时间 %@", [format stringFromDate:date]];
    [[LdsdSipManager manager] ldsdSendMessage:self.sipNum message:message];
}

- (IBAction)respondsToSnapshot:(id)sender {
    NSString *name = [NSString stringWithFormat:@"%.0f.jpg", [NSDate date].timeIntervalSince1970];
    NSString *cache = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject;
    NSString *file = [cache stringByAppendingPathComponent:name];
    // 抓拍 0成功
    int a = [[LdsdSipManager manager] ldsdSnapShot:file];
    
    // 查看本地文件
    NSArray *arr = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cache error:nil];
    NSLog(@"\n\n\n\n a = %d, arr = %@ \n\n\n\n", a, arr);
}

- (IBAction)respondsToCloseOrOpenVideo:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    LinphoneCall *call = linphone_core_get_current_call(LC);
    const LinphoneCallParams *cp = linphone_call_get_current_params(call);
    LinphoneCallParams *newCP = linphone_call_params_copy(cp);
    
    if (sender.selected) {
        linphone_call_params_set_video_direction(newCP, LinphoneMediaDirectionInvalid);
    }else {
        linphone_call_params_set_video_direction(newCP, LinphoneMediaDirectionRecvOnly);
    }
    
    int result = linphone_call_update(call, newCP);
    
    linphone_call_params_unref(newCP);
    NSLog(@"result = %d", result);
    
    return;
    /*
    // 发送消息
    LinphoneInfoMessage *im = linphone_core_create_info_message(LC);
    LinphoneContent *content = linphone_core_create_content(LC);
    linphone_content_set_string_buffer(content, "aaaa");
    linphone_info_message_set_content(im, content);
    LinphoneCall *call = linphone_core_get_current_call(LC);
    int a =  linphone_call_send_info_message(call, im);
    NSLog(@"\n\n\n respondsToCloseOrOpenVideo a = %d \n\n\n", a);
     */
}

- (void)dealloc {
    NSLog(@"\n\n\n CallVC dealloc \n\n\n");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end















