//
//  XQPhotosVC.m
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/5.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import "XQPhotosVC.h"
#import "XQPhotosCell.h"
#import "XQImgVC.h"

static NSString *reusing_ = @"XQPhotosCell";

@interface XQPhotosVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *dataArr;

@end

@implementation XQPhotosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *cache = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject;
    NSArray *arr = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cache error:nil];
    
    NSMutableArray *muArr = [NSMutableArray array];
    for (NSString *name in arr) {
        if ([name hasSuffix:@".jpg"]) {
            [muArr addObject:[cache stringByAppendingPathComponent:name]];
        }
    }
    
    self.dataArr = muArr;
    
    [self.view addSubview:self.tableView];
}

#pragma mark -- UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XQPhotosCell *cell = [tableView dequeueReusableCellWithIdentifier:reusing_ forIndexPath:indexPath];
    
    UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfFile:self.dataArr[indexPath.row]]];
    cell.imageView.image = img;
    
    return cell;
}

#pragma mark -- UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    XQImgVC *vc = [[XQImgVC alloc] initWithNibName:NSStringFromClass([XQImgVC class]) bundle:nil];
    UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfFile:self.dataArr[indexPath.row]]];
    vc.img = img;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- get

- (UITableView *)tableView {
    
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        UINib *nib = [UINib nibWithNibName:NSStringFromClass([XQPhotosCell class]) bundle:nil];
        [_tableView registerNib:nib forCellReuseIdentifier:reusing_];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = 80;
        
    }
    return _tableView;
}

- (NSArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSArray array];
    }
    return _dataArr;
}


@end









