//
//  XQPhotosCell.h
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/5.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XQPhotosCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end
