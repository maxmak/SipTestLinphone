//
//  XQImgVC.m
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/5.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import "XQImgVC.h"

@interface XQImgVC ()

@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end

@implementation XQImgVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.img) {
        self.imgView.image = self.img;
    }
}



@end










