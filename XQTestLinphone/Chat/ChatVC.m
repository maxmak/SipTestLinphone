//
//  ChatVC.m
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/10.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import "ChatVC.h"
#import "ChatCell.h"
#import "XQChatMessageModel.h"
#import "InputBoxView.h"

#import "LinphoneManager.h"

static NSString *reusing_ = @"ChatCell";

@interface ChatVC () <UITableViewDelegate, UITableViewDataSource, LdsdSipManagerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray <XQChatMessageModel *> *dataArr;

@property (nonatomic, strong) InputBoxView *inputBoxView;

/** 当时接收消息，和发送消息, 就不用结束编辑状态 */
@property (nonatomic, assign) BOOL isEndEdit;

@end

@implementation ChatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"聊天室";
    
    [LdsdSipManager manager].delegate = self;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.inputBoxView];
    
    Weak_Self
    self.inputBoxView.block = ^(NSString *message) {
        [weakSelf sendWithMessage:message];
    };
    
    self.isEndEdit = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShowNotification:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Get" style:UIBarButtonItemStylePlain target:self action:@selector(getHistoryMessage)];
}

- (void)getHistoryMessage {
    // 地址字符串: sip:username@domain
    const char *identity = [NSString stringWithFormat:@"sip:%@@%@", self.sipNum, self.domain].UTF8String;
    LinphoneChatRoom *chatRoom = linphone_core_get_chat_room_from_uri(LC, identity);
    
    // 获取聊天记录
    bctbx_list_t *historyList = linphone_chat_room_get_history(chatRoom, 10);
    
    while (historyList) {
        LinphoneChatMessage *historyMessage = linphone_chat_message_ref(historyList->data);
//        linphone_chat_room_set_user_data(chatRoom, historyMessage);
        const char *text = linphone_chat_message_get_text(historyMessage);
        NSLog(@"text = %s", text);
        
        // 获取下一个list, 也就是下一个聊天记录
        historyList = historyList->next;
    }
    
    bctbx_list_free(historyList);
}

- (void)sendWithMessage:(NSString *)message {
    if (!message || [message isEqualToString:@""]) {
        return;
    }
    
    NSString *messageID = [[LdsdSipManager manager] ldsdSendMessage:self.sipNum message:message];
    
    if (!messageID || [messageID isEqualToString:@""]) {
        NSLog(@"messageID not exist");
        return;
    }
    
    self.inputBoxView.textView.text = @"";
    
    XQChatMessageModel *model = [XQChatMessageModel new];
    model.fromUserName = self.selfSipNum;
    model.fromDisplayName = self.selfDisplayName;
    model.toUserName = self.sipNum;
    model.message = message;
    model.messageID = messageID;
    model.isSelf = YES;
    model.height = [XQChatMessageModel heightWithFrom:self.selfSipNum message:message];
    
    [self.dataArr addObject:model];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.dataArr.count - 1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
    
    [self scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)scrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated {
    self.isEndEdit = NO;
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:animated];
}

#pragma mark -- notice

- (void)keyboardDidShowNotification:(NSNotification *)notification {
    NSDictionary *dic = notification.userInfo;
    
//    NSTimeInterval duration = ((NSString *)dic[UIKeyboardAnimationDurationUserInfoKey]).floatValue;
    NSValue *endValue = dic[UIKeyboardFrameEndUserInfoKey];
    CGRect endRect = [endValue CGRectValue];;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.inputBoxView.y = endRect.origin.y - self.inputBoxView.height;
        self.tableView.frame = CGRectMake(0, 64, Screen_Width, Screen_Height - 64 - self.inputBoxView.height - endRect.size.height);
    }];
}

- (void)keyboardWillHideNotification:(NSNotification *)notification {
    NSDictionary *dic = notification.userInfo;
    
    NSTimeInterval duration = ((NSString *)dic[UIKeyboardAnimationDurationUserInfoKey]).floatValue;
    
    [UIView animateWithDuration:duration animations:^{
        self.inputBoxView.y = Screen_Height - _inputBoxView.height;
        self.tableView.frame = CGRectMake(0, 64, Screen_Width, Screen_Height - 64 - self.inputBoxView.height);
    }];
}

#pragma mark -- LdsdSipManagerDelegate

- (void)ldsdReceiveMessageWithFrom:(NSString *)from to:(NSString *)to displayName:(NSString *)displayName message:(NSString *)message messageID:(NSString *)messageID {
    XQChatMessageModel *model = [XQChatMessageModel new];
    model.fromUserName = from;
    model.fromDisplayName = displayName;
    model.toUserName = to;
    model.toDisplayName = self.selfDisplayName;
    model.message = message;
    model.messageID = messageID;
    model.isSelf = NO;
    model.height = [XQChatMessageModel heightWithFrom:from message:message];
    
    [self.dataArr addObject:model];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.dataArr.count - 1 inSection:0];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        [self scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });
}

- (void)ldsdSendMessageWithFrom:(NSString *)from to:(NSString *)to status:(LdsdSipChatMessageState)status message:(NSString *)message messageID:(NSString *)messageID {
    NSPredicate *preicate = [NSPredicate predicateWithFormat:@"_messageID CONTAINS[cd] %@", messageID];
    NSArray *arr = [self.dataArr filteredArrayUsingPredicate:preicate];
    if (!arr || arr.count == 0) {
        return;
    }
    
    NSUInteger index = [self.dataArr indexOfObject:arr.firstObject];
    self.dataArr[index].status = (XQChatMessageSendStatus)status;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *indxePath = [NSIndexPath indexPathForRow:index inSection:0];
        ChatCell *cell = [self.tableView cellForRowAtIndexPath:indxePath];
        if (cell) {
            cell.status = (XQChatMessageSendStatus)status;
        }
    });
    
}

#pragma mark -- UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatCell *cell = [tableView dequeueReusableCellWithIdentifier:reusing_ forIndexPath:indexPath];
    
    XQChatMessageModel *model = self.dataArr[indexPath.row];
    
    cell.nameLabel.text = model.fromDisplayName;
    cell.name = model.fromDisplayName;
    cell.messageLabel.text = model.message;
    
    cell.isSelf = model.isSelf;
    cell.messageID = model.messageID;
    
    cell.status = model.status;
    
    return cell;
}

#pragma mark -- UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.dataArr[indexPath.row].height;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.isEndEdit) {
        [self.inputBoxView.textView endEditing:YES];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    self.isEndEdit = YES;
}

#pragma mark -- get

- (UITableView *)tableView {
    
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, Screen_Width, Screen_Height - 64 - self.inputBoxView.height) style:UITableViewStylePlain];
        UINib *nib = [UINib nibWithNibName:NSStringFromClass([ChatCell class]) bundle:nil];
        [_tableView registerNib:nib forCellReuseIdentifier:reusing_];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor lightGrayColor];
        
    }
    return _tableView;
}

- (NSMutableArray<XQChatMessageModel *> *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (InputBoxView *)inputBoxView {
    if (!_inputBoxView) {
        _inputBoxView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([InputBoxView class]) owner:self options:nil].firstObject;
        CGFloat height = 60;
        _inputBoxView.frame = CGRectMake(0, Screen_Height - height, Screen_Width, height);
        _inputBoxView.backgroundColor = [UIColor orangeColor];
    }
    return _inputBoxView;
}

- (void)dealloc {
    NSLog(@"ChatVC dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end














