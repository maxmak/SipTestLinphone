//
//  InputBoxView.m
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/11.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import "InputBoxView.h"

@implementation InputBoxView

- (IBAction)respondsToSend:(id)sender {
    if (self.block) {
        self.block(self.textView.text);
    }
}

@end
