//
//  InputBoxView.h
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/11.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SendBlock)(NSString *message);

@interface InputBoxView : UIView

@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (nonatomic, copy) SendBlock block;

@end
