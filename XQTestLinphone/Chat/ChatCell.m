//
//  ChatCell.m
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/10.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import "ChatCell.h"

@implementation ChatCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setIsSelf:(BOOL)isSelf {
    _isSelf = isSelf;
    NSTextAlignment alignment = NSTextAlignmentLeft;
    NSUInteger messageleftCons = 10;
    NSUInteger messageRightCons = 10;
    
    if (isSelf) {
        alignment = NSTextAlignmentRight;
        messageleftCons = 70;
        
    }else {
        messageRightCons = 70;
        
    }
    
    self.messageLeftCons.constant = messageleftCons;
    self.messageRightCons.constant = messageRightCons;
    
    self.nameLabel.textAlignment = alignment;
    self.messageLabel.textAlignment = alignment;
}

- (void)setStatus:(XQChatMessageSendStatus)status {
    _status = status;
    if (!self.isSelf) {
        return;
    }
    
    NSString *statusStr = @"";
    switch (status) {
        case XQChatMessageSendStatusNone:{
            statusStr = @"未知状态  ";
        }
            break;
            
        case XQChatMessageSendStatusInProgress:{
            statusStr = @"正在发送  ";
        }
            break;
            
        case XQChatMessageSendStatusDelivered:{
            statusStr = @"发送成功  ";
        }
            break;
            
        case XQChatMessageSendStatusNotDelivered:{
            statusStr = @"发送失败  ";
        }
            break;
            
        default:
            break;
    }
    
    self.nameLabel.text = [statusStr stringByAppendingString:self.name];
}

@end















