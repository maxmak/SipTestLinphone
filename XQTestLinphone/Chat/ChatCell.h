//
//  ChatCell.h
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/10.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, copy) NSString *name;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageLeftCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageRightCons;

@property (nonatomic, assign) BOOL isSelf;

@property (nonatomic, assign) XQChatMessageSendStatus status;

@property (nonatomic, copy) NSString *messageID;

@end
