//
//  XQChatMessageModel.m
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/10.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import "XQChatMessageModel.h"

@implementation XQChatMessageModel

+ (CGFloat)heightWithFrom:(NSString *)from message:(NSString *)message {
    CGFloat top = 40;
    CGFloat height = top;
    
    if (!from || [from isEqualToString:@""] || !message || [message isEqualToString:@""]) {
        return height;
    }
    
    CGFloat width = Screen_Width - 70 - 10;
    CGFloat messageHeight = [message boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil].size.height;
    
    height = top + messageHeight + 10;
    
    return height;
}

@end
