//
//  ChatVC.h
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/10.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatVC : UIViewController

@property (nonatomic, copy) NSString *sipNum;

@property (nonatomic, copy) NSString *selfSipNum;
@property (nonatomic, copy) NSString *selfDisplayName;

@property (nonatomic, copy) NSString *domain;

@end
