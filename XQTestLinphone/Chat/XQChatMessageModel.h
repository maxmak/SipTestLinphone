//
//  XQChatMessageModel.h
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/10.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XQChatMessageModel : NSObject

/** 发送消息人的userName */
@property (nonatomic, copy) NSString *fromUserName;
/** 发送人displayName */
@property (nonatomic, copy) NSString *fromDisplayName;

/** 接收人消息人的userName */
@property (nonatomic, copy) NSString *toUserName;
/** 接收人displayName */
@property (nonatomic, copy) NSString *toDisplayName;

/** 消息 */
@property (nonatomic, copy) NSString *message;
/** 消息标志 */
@property (nonatomic, copy) NSString *messageID;

/** 是否自己发送, YES是 */
@property (nonatomic, assign) BOOL isSelf;

/** 发送状态 */
@property (nonatomic, assign) XQChatMessageSendStatus status;

/** 高 */
@property (nonatomic, assign) CGFloat height;

+ (CGFloat)heightWithFrom:(NSString *)from message:(NSString *)message;

@end










