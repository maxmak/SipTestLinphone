//
//  AppDelegate.h
//  XQTestLinphone
//
//  Created by ladystyle100 on 2017/8/12.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

