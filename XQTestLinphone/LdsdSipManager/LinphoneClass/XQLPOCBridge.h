//
//  XQLPOCBridge.h
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/10.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <linphone/linphonecore.h>

@interface XQLPOCBridge : NSObject

/** 获取displayName */
+ (NSString *)getDisplayNameWithAddress:(const LinphoneAddress *)address;

/** 获取userName */
+ (NSString *)getUserNameWithAddress:(const LinphoneAddress *)address;

/** 获取domain */
+ (NSString *)getDomainWithAddress:(const LinphoneAddress *)address;

/** 获取消息 */
+ (NSString *)getMessageWithChatMessage:(const LinphoneChatMessage *)chatMessage;

/** 获取messageID */
+ (NSString *)getMessageIDWithChatMessage:(const LinphoneChatMessage *)chatMessage;

@end
