
/* LinphoneManager.h
 *
 * Copyright (C) 2011  Belledonne Comunications, Grenoble, France
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#import "LinphoneManager.h"

#define LINPHONE_LOGS_MAX_ENTRY 5000

static LinphoneCore *theLinphoneCore = nil;
static LinphoneManager *theLinphoneManager = nil;

NSString *const LINPHONERC_APPLICATION_KEY = @"app";

NSString *const kLinphoneCoreUpdate                          = @"ldsdLinphoneCoreUpdate";                          //
NSString *const kLinphoneDisplayStatusUpdate                 = @"ldsdLinphoneDisplayStatusUpdate";                 //显示状态
NSString *const kLinphoneMessageReceived                     = @"ldsdLinphoneMessageReceived";                     //接收到信息
NSString *const kLinphoneTextComposeEvent                    = @"ldsdLinphoneTextComposeStarted";                  //开始编辑文字
NSString *const kLinphoneCallChange                          = @"ldsdLinphoneCallChange";                          //通话状态改变
NSString *const kLinphoneCallUpdate                          = @"ldsdLinphoneCallUpdate";                          //通话更新
NSString *const kLinphoneRegistrationUpdate                  = @"ldsdLinphoneRegistrationUpdate";                  //注册更新
NSString *const kLinphoneAddressBookUpdate                   = @"ldsdLinphoneAddressBookUpdate";                   //通讯录更新
NSString *const kLinphoneMainViewChange                      = @"ldsdLinphoneMainViewChange";                      //主视图更新
NSString *const kLinphoneLogsUpdate                          = @"ldsdLinphoneLogsUpdate";                          //日志更新
NSString *const kLinphoneSettingsUpdate                      = @"ldsdLinphoneSettingsUpdate";                      //设置更新
NSString *const kLinphoneBluetoothAvailabilityUpdate         = @"ldsdLinphoneBluetoothAvailabilityUpdate";         //蓝牙可用性更新
NSString *const kLinphoneConfiguringStateUpdate              = @"ldsdLinphoneConfiguringStateUpdate";              //配置状态更新
NSString *const kLinphoneGlobalStateUpdate                   = @"ldsdLinphoneGlobalStateUpdate";                   //全局状态更新
NSString *const kLinphoneNotifyReceived                      = @"ldsdLinphoneNotifyReceived";                      //接收到通知
NSString *const kLinphoneNotifyPresenceReceivedForUriOrTel   = @"ldsdLinphoneNotifyPresenceReceivedForUriOrTel";     //
NSString *const kLinphoneCallEncryptionChanged               = @"ldsdLinphoneCallEncryptionChanged";               //通话加密状态改变
NSString *const kLinphoneFileTransferSendUpdate              = @"ldsdLinphoneFileTransferSendUpdate";              //文件传输发送更新
NSString *const kLinphoneFileTransferRecvUpdate              = @"ldsdLinphoneFileTransferRecvUpdate";              //文件传输接受更新

const int kLinphoneAudioVbrCodecDefaultBitrate = 36; /*you can override this from linphonerc or linphonerc-factory*/

extern void libmsamr_init(MSFactory *factory);
extern void libmsx264_init(MSFactory *factory);
extern void libmsopenh264_init(MSFactory *factory);
extern void libmssilk_init(MSFactory *factory);
extern void libmsbcg729_init(MSFactory *factory);
extern void libmswebrtc_init(MSFactory *factory);

NSString *const kLinphoneOldChatDBFilename = @"chat_database.sqlite";
NSString *const kLinphoneInternalChatDBFilename = @"linphone_chats.db";

@implementation LinphoneCallAppData

- (id)init {
	if ((self = [super init])) {
		batteryWarningShown = FALSE;
		notification = nil;
		videoRequested = FALSE;
		userInfos = [[NSMutableDictionary alloc] init];
	}
	return self;
}

@end

@interface LinphoneManager ()

@end

@implementation LinphoneManager

@synthesize connectivity;

struct codec_name_pref_table {
	const char *name;
	int rate;
	const char *prefname;
};

struct codec_name_pref_table codec_pref_table[] = {{"speex", 8000, "speex_8k_preference"},
												   {"speex", 16000, "speex_16k_preference"},
												   {"silk", 24000, "silk_24k_preference"},
												   {"silk", 16000, "silk_16k_preference"},
												   {"amr", 8000, "amr_preference"},
												   {"gsm", 8000, "gsm_preference"},
												   {"ilbc", 8000, "ilbc_preference"},
												   {"isac", 16000, "isac_preference"},
												   {"pcmu", 8000, "pcmu_preference"},
												   {"pcma", 8000, "pcma_preference"},
												   {"g722", 8000, "g722_preference"},
												   {"g729", 8000, "g729_preference"},
												   {"mp4v-es", 90000, "mp4v-es_preference"},
												   {"h264", 90000, "h264_preference"},
												   {"vp8", 90000, "vp8_preference"},
												   {"mpeg4-generic", 16000, "aaceld_16k_preference"},
												   {"mpeg4-generic", 22050, "aaceld_22k_preference"},
												   {"mpeg4-generic", 32000, "aaceld_32k_preference"},
												   {"mpeg4-generic", 44100, "aaceld_44k_preference"},
												   {"mpeg4-generic", 48000, "aaceld_48k_preference"},
												   {"opus", 48000, "opus_preference"},
												   {NULL, 0, Nil}};

+ (NSString *)getPreferenceForCodec:(const char *)name withRate:(int)rate {
	int i;
	for (i = 0; codec_pref_table[i].name != NULL; ++i) {
		if (strcasecmp(codec_pref_table[i].name, name) == 0 && codec_pref_table[i].rate == rate)
			return [NSString stringWithUTF8String:codec_pref_table[i].prefname];
	}
	return Nil;
}

+ (NSSet *)unsupportedCodecs {
	NSMutableSet *set = [NSMutableSet set];
	for (int i = 0; codec_pref_table[i].name != NULL; ++i) {
		LinphonePayloadType *available = linphone_core_get_payload_type(
			theLinphoneCore, codec_pref_table[i].name, codec_pref_table[i].rate, LINPHONE_FIND_PAYLOAD_IGNORE_CHANNELS);
		if ((available == NULL)
			// these two codecs should not be hidden, even if not supported
			&& strcmp(codec_pref_table[i].prefname, "h264_preference") != 0 &&
			strcmp(codec_pref_table[i].prefname, "mp4v-es_preference") != 0) {
			[set addObject:[NSString stringWithUTF8String:codec_pref_table[i].prefname]];
		}
	}
	return set;
}

+ (BOOL)isCodecSupported:(const char *)codecName {
	return (codecName != NULL) &&
		   (NULL != linphone_core_get_payload_type(theLinphoneCore, codecName, LINPHONE_FIND_PAYLOAD_IGNORE_RATE,
													LINPHONE_FIND_PAYLOAD_IGNORE_CHANNELS));
}

+ (BOOL)runningOnIpad {
    // 设备类型
	return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
}

+ (BOOL)isRunningTests {
	NSDictionary *environment = [[NSProcessInfo processInfo] environment];
	NSString *injectBundle = environment[@"XCInjectBundle"];
	return [[injectBundle pathExtension] isEqualToString:@"xctest"];
}

+ (BOOL)isNotIphone3G {
	static BOOL done = FALSE;
	static BOOL result;
	if (!done) {
		size_t size;
		sysctlbyname("hw.machine", NULL, &size, NULL, 0);
		char *machine = malloc(size);
		sysctlbyname("hw.machine", machine, &size, NULL, 0);
		NSString *platform = [[NSString alloc] initWithUTF8String:machine];
		free(machine);
        
		result = ![platform isEqualToString:@"iPhone1,2"];

		done = TRUE;
	}
	return result;
}

+ (NSString *)getUserAgent {
	return
		[NSString stringWithFormat:@"LinphoneIphone/%@ (Linphone/%s; Apple %@/%@)",
								   [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey],
								   linphone_core_get_version(), [UIDevice currentDevice].systemName,
								   [UIDevice currentDevice].systemVersion];
}

+ (LinphoneManager *)instance {
    if (theLinphoneManager == nil) {
        theLinphoneManager = [[LinphoneManager alloc] init];
    }
    
	return theLinphoneManager;
}

#ifdef DEBUG
+ (void)instanceRelease {
	if (theLinphoneManager != nil) {
		theLinphoneManager = nil;
	}
}
#endif

+ (BOOL)langageDirectionIsRTL {
    // 文字书写方向
	static NSLocaleLanguageDirection dir = NSLocaleLanguageDirectionLeftToRight;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
	  dir = [NSLocale characterDirectionForLanguage:[[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode]];
	});
	return dir == NSLocaleLanguageDirectionRightToLeft;
}

#pragma mark - Lifecycle Functions

- (id)init {
	if ((self = [super init])) {
        // 监听声音通道
		[NSNotificationCenter.defaultCenter addObserver:self
											   selector:@selector(audioRouteChangeListenerCallback:)
												   name:AVAudioSessionRouteChangeNotification
												 object:nil];
        // 默认打印是debug
        self.logLevel = ORTP_DEBUG;
        
		_sounds.vibrate = kSystemSoundID_Vibrate;
        
		_database = NULL;
		_speakerEnabled = FALSE;
		_bluetoothEnabled = FALSE;
        
		_isTesting = [LinphoneManager isRunningTests];
        
        // 重置本地文件
		[self renameDefaultSettings];
        // 赋值默认设置
		[self copyDefaultSettings];
		[self overrideDefaultSettings];
        
        
		// set default values for first boot
		if ([self lpConfigStringForKey:@"debugenable_preference"] == nil) {
#ifdef DEBUG
			[self lpConfigSetInt:1 forKey:@"debugenable_preference"];
#else
			[self lpConfigSetInt:0 forKey:@"debugenable_preference"];
#endif
		}

		// by default if handle_content_encoding is not set, we use plain text for debug purposes only
		if ([self lpConfigStringForKey:@"handle_content_encoding" inSection:@"misc"] == nil) {
#ifdef DEBUG
			[self lpConfigSetString:@"none" forKey:@"handle_content_encoding" inSection:@"misc"];
#else
			[self lpConfigSetString:@"conflate" forKey:@"handle_content_encoding" inSection:@"misc"];
#endif
		}

		[self migrateFromUserPrefs];
	}
	return self;
}

- (void)dealloc {
	[NSNotificationCenter.defaultCenter removeObserver:self];
}

#pragma mark - Migration

- (void)migrationAllPost {
	[self migrationLinphoneSettings];
}

- (void)migrationAllPre {
	// migrate xmlrpc URL if needed
	if ([self lpConfigBoolForKey:@"migration_xmlrpc"] == NO) {
		[self lpConfigSetString:@"https://subscribe.linphone.org:444/wizard.php"
						 forKey:@"xmlrpc_url"
					  inSection:@"assistant"];
		[self lpConfigSetString:@"sip:rls@sip.linphone.org" forKey:@"rls_uri" inSection:@"sip"];
		[self lpConfigSetBool:YES forKey:@"migration_xmlrpc"];
	}
	[self lpConfigSetBool:NO forKey:@"store_friends" inSection:@"misc"]; //so far, storing friends in files is not needed. may change in the future.
}

static int check_should_migrate_images(void *data, int argc, char **argv, char **cnames) {
	*((BOOL *)data) = TRUE;
	return 0;
}

- (BOOL)migrateChatDBIfNeeded:(LinphoneCore *)lc {
	sqlite3 *newDb;
	char *errMsg;
	NSError *error;
	NSString *oldDbPath = [LinphoneManager documentFile:kLinphoneOldChatDBFilename];
	NSString *newDbPath = [LinphoneManager documentFile:kLinphoneInternalChatDBFilename];
	BOOL shouldMigrate = [[NSFileManager defaultManager] fileExistsAtPath:oldDbPath];
	BOOL shouldMigrateImages = FALSE;
	const char *identity = NULL;
	BOOL migrated = FALSE;
	char *attach_stmt = NULL;
	LinphoneProxyConfig *default_proxy = linphone_core_get_default_proxy_config(lc);

	if (sqlite3_open([newDbPath UTF8String], &newDb) != SQLITE_OK) {
		LOGE(@"Can't open \"%@\" sqlite3 database.", newDbPath);
		return FALSE;
	}

	const char *check_appdata =
		"SELECT url,message FROM history WHERE url LIKE 'assets-library%' OR message LIKE 'assets-library%' LIMIT 1;";
	// will set "needToMigrateImages to TRUE if a result comes by
	sqlite3_exec(newDb, check_appdata, check_should_migrate_images, &shouldMigrateImages, NULL);
	if (!shouldMigrate && !shouldMigrateImages) {
		sqlite3_close(newDb);
		return FALSE;
	}

	LOGI(@"Starting migration procedure");

	if (shouldMigrate) {

		// attach old database to the new one:
		attach_stmt = sqlite3_mprintf("ATTACH DATABASE %Q AS oldchats", [oldDbPath UTF8String]);
		if (sqlite3_exec(newDb, attach_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
			LOGE(@"Can't attach old chat table, error[%s] ", errMsg);
			sqlite3_free(errMsg);
			goto exit_dbmigration;
		}

		// migrate old chats to the new db. The iOS stores timestamp in UTC already, so we can directly put it in the
		// 'utc' field and set 'time' to -1
		const char *migration_statement =
			"INSERT INTO history (localContact,remoteContact,direction,message,utc,read,status,time) "
			"SELECT localContact,remoteContact,direction,message,time,read,state,'-1' FROM oldchats.chat";

		if (sqlite3_exec(newDb, migration_statement, NULL, NULL, &errMsg) != SQLITE_OK) {
			LOGE(@"DB migration failed, error[%s] ", errMsg);
			sqlite3_free(errMsg);
			goto exit_dbmigration;
		}

		// invert direction of old messages, because iOS was storing the direction flag incorrectly
		const char *invert_direction = "UPDATE history SET direction = NOT direction";
		if (sqlite3_exec(newDb, invert_direction, NULL, NULL, &errMsg) != SQLITE_OK) {
			LOGE(@"Inverting direction failed, error[%s]", errMsg);
			sqlite3_free(errMsg);
			goto exit_dbmigration;
		}

		// replace empty from: or to: by the current identity.
		if (default_proxy) {
			identity = linphone_proxy_config_get_identity(default_proxy);
		}
		if (!identity) {
			identity = "sip:unknown@sip.linphone.org";
		}

		char *from_conversion =
			sqlite3_mprintf("UPDATE history SET localContact = %Q WHERE localContact = ''", identity);
		if (sqlite3_exec(newDb, from_conversion, NULL, NULL, &errMsg) != SQLITE_OK) {
			LOGE(@"FROM conversion failed, error[%s] ", errMsg);
			sqlite3_free(errMsg);
		}
		sqlite3_free(from_conversion);

		char *to_conversion =
			sqlite3_mprintf("UPDATE history SET remoteContact = %Q WHERE remoteContact = ''", identity);
		if (sqlite3_exec(newDb, to_conversion, NULL, NULL, &errMsg) != SQLITE_OK) {
			LOGE(@"DB migration failed, error[%s] ", errMsg);
			sqlite3_free(errMsg);
		}
		sqlite3_free(to_conversion);
	}

	// local image paths were stored in the 'message' field historically. They were
	// very temporarily stored in the 'url' field, and now we migrated them to a JSON-
	// encoded field. These are the migration steps to migrate them.

	// move already stored images from the messages to the appdata JSON field
	const char *assetslib_migration = "UPDATE history SET appdata='{\"localimage\":\"'||message||'\"}' , message='' "
									  "WHERE message LIKE 'assets-library%'";
	if (sqlite3_exec(newDb, assetslib_migration, NULL, NULL, &errMsg) != SQLITE_OK) {
		LOGE(@"Assets-history migration for MESSAGE failed, error[%s] ", errMsg);
		sqlite3_free(errMsg);
	}

	// move already stored images from the url to the appdata JSON field
	const char *assetslib_migration_fromurl =
		"UPDATE history SET appdata='{\"localimage\":\"'||url||'\"}' , url='' WHERE url LIKE 'assets-library%'";
	if (sqlite3_exec(newDb, assetslib_migration_fromurl, NULL, NULL, &errMsg) != SQLITE_OK) {
		LOGE(@"Assets-history migration for URL failed, error[%s] ", errMsg);
		sqlite3_free(errMsg);
	}

	// We will lose received messages with remote url, they will be displayed in plain. We can't do much for them..
	migrated = TRUE;

exit_dbmigration:

	if (attach_stmt)
		sqlite3_free(attach_stmt);

	sqlite3_close(newDb);

	// in any case, we should remove the old chat db
	if (shouldMigrate && ![[NSFileManager defaultManager] removeItemAtPath:oldDbPath error:&error]) {
		LOGE(@"Could not remove old chat DB: %@", error);
	}

	LOGI(@"Message storage migration finished: success = %@", migrated ? @"TRUE" : @"FALSE");
	return migrated;
}

- (void)migrateFromUserPrefs {
	static NSString *migration_flag = @"userpref_migration_done";

	if (_configDb == nil)
		return;

	if ([self lpConfigIntForKey:migration_flag withDefault:0]) {
		return;
	}

	NSDictionary *defaults = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
	NSArray *defaults_keys = [defaults allKeys];
	NSDictionary *values =
		@{ @"backgroundmode_preference" : @YES,
		   @"debugenable_preference" : @NO,
		   @"start_at_boot_preference" : @YES };
	BOOL shouldSync = FALSE;

	LOGI(@"%lu user prefs", (unsigned long)[defaults_keys count]);

	for (NSString *userpref in values) {
		if ([defaults_keys containsObject:userpref]) {
			LOGI(@"Migrating %@ from user preferences: %d", userpref, [[defaults objectForKey:userpref] boolValue]);
			[self lpConfigSetBool:[[defaults objectForKey:userpref] boolValue] forKey:userpref];
			[[NSUserDefaults standardUserDefaults] removeObjectForKey:userpref];
			shouldSync = TRUE;
		} else if ([self lpConfigStringForKey:userpref] == nil) {
			// no default value found in our linphonerc, we need to add them
			[self lpConfigSetBool:[[values objectForKey:userpref] boolValue] forKey:userpref];
		}
	}

	if (shouldSync) {
		LOGI(@"Synchronizing...");
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
	// don't get back here in the future
	[self lpConfigSetBool:YES forKey:migration_flag];
}

- (void)migrationLinphoneSettings {
	// we need to proceed to the migration *after* the chat database was opened, so that we know it is in consistent
	// state
	NSString *chatDBFileName = [LinphoneManager documentFile:kLinphoneInternalChatDBFilename];
	if ([self migrateChatDBIfNeeded:theLinphoneCore]) {
		// if a migration was performed, we should reinitialize the chat database
		linphone_core_set_chat_database_path(theLinphoneCore, [chatDBFileName UTF8String]);
	}

	/* AVPF migration */
	if ([self lpConfigBoolForKey:@"avpf_migration_done"] == FALSE) {
		const MSList *proxies = linphone_core_get_proxy_config_list(theLinphoneCore);
		while (proxies) {
			LinphoneProxyConfig *proxy = (LinphoneProxyConfig *)proxies->data;
			const char *addr = linphone_proxy_config_get_addr(proxy);
			// we want to enable AVPF for the proxies
			if (addr && strstr(addr, "sip.linphone.org") != 0) {
				LOGI(@"Migrating proxy config to use AVPF");
				linphone_proxy_config_set_avpf_mode(proxy, LinphoneAVPFEnabled);
			}
			proxies = proxies->next;
		}
		[self lpConfigSetBool:TRUE forKey:@"avpf_migration_done"];
	}
	/* Quality Reporting migration */
	if ([self lpConfigBoolForKey:@"quality_report_migration_done"] == FALSE) {
		const MSList *proxies = linphone_core_get_proxy_config_list(theLinphoneCore);
		while (proxies) {
			LinphoneProxyConfig *proxy = (LinphoneProxyConfig *)proxies->data;
			const char *addr = linphone_proxy_config_get_addr(proxy);
			// we want to enable quality reporting for the proxies that are on linphone.org
			if (addr && strstr(addr, "sip.linphone.org") != 0) {
				LOGI(@"Migrating proxy config to send quality report");
				linphone_proxy_config_set_quality_reporting_collector(
					proxy, "sip:voip-metrics@sip.linphone.org;transport=tls");
				linphone_proxy_config_set_quality_reporting_interval(proxy, 180);
				linphone_proxy_config_enable_quality_reporting(proxy, TRUE);
			}
			proxies = proxies->next;
		}
		[self lpConfigSetBool:TRUE forKey:@"quality_report_migration_done"];
	}
	/* File transfer migration */
	if ([self lpConfigBoolForKey:@"file_transfer_migration_done"] == FALSE) {
		const char *newURL = "https://www.linphone.org:444/lft.php";
		LOGI(@"Migrating sharing server url from %s to %s", linphone_core_get_file_transfer_server(LC), newURL);
		linphone_core_set_file_transfer_server(LC, newURL);
		[self lpConfigSetBool:TRUE forKey:@"file_transfer_migration_done"];
	}
}

static void migrateWizardToAssistant(const char *entry, void *user_data) {
	LinphoneManager *thiz = (__bridge LinphoneManager *)(user_data);
	NSString *key = [NSString stringWithUTF8String:entry];
	[thiz lpConfigSetString:[thiz lpConfigStringForKey:key inSection:@"wizard"] forKey:key inSection:@"assistant"];
}

#pragma mark - Linphone Core Functions

+ (LinphoneCore *)getLc {
	if (theLinphoneCore == nil) {
//		@throw([NSException exceptionWithName:@"LinphoneCoreException" reason:@"Linphone core not initialized yet" userInfo:nil]);
	}
	return theLinphoneCore;
}

#pragma mark Debug functions

+ (void)dumpLcConfig {
	if (theLinphoneCore) {
		LpConfig *conf = LinphoneManager.instance.configDb;
		char *config = lp_config_dump(conf);
		LOGI(@"\n%s", config);
		ms_free(config);
	}
}

#pragma mark - Logs Functions handlers
static void linphone_iphone_log_user_info(struct _LinphoneCore *lc, const char *message) {
	linphone_iphone_log_handler(NULL, ORTP_MESSAGE, message, NULL);
}
static void linphone_iphone_log_user_warning(struct _LinphoneCore *lc, const char *message) {
	linphone_iphone_log_handler(NULL, ORTP_WARNING, message, NULL);
}

#pragma mark - Display Status Functions

- (void)displayStatus:(NSString *)message {
    LOG(@"\n\n\n displayStatus message = %@ \n\n\n", message);
	// Post event
	[NSNotificationCenter.defaultCenter postNotificationName:kLinphoneDisplayStatusUpdate
													  object:self
													userInfo:@{
														@"message" : message
													}];
}

static void linphone_iphone_display_status(struct _LinphoneCore *lc, const char *message) {
	NSString *status = [[NSString alloc] initWithCString:message encoding:[NSString defaultCStringEncoding]];
	[LinphoneManager.instance displayStatus:status];
}

#pragma mark - Call State Functions

- (void)localNotifContinue:(NSTimer *)timer {
	UILocalNotification *notif = [timer userInfo];
	if (notif) {
		LOGI(@"cancelling/presenting local notif");
		[[UIApplication sharedApplication] cancelAllLocalNotifications];
		[[UIApplication sharedApplication] presentLocalNotificationNow:notif];
	}
}

// 通话状态改变通知
- (void)onCall:(LinphoneCall *)call StateChanged:(LinphoneCallState)state withMessage:(const char *)message {
	// Handling wrapper
    
	LinphoneCallAppData *data = (__bridge LinphoneCallAppData *)linphone_call_get_user_data(call);
	if (!data) {
		data = [[LinphoneCallAppData alloc] init];
		linphone_call_set_user_data(call, (void *)CFBridgingRetain(data));
	}

#pragma deploymate push "ignored-api-availability"
	
#pragma deploymate pop
    // 获取对方的地址
//	const LinphoneAddress *addr = linphone_call_get_remote_address(call);
//    NSString *opponent = [XQLPOCBridge getUserNameWithAddress:addr];
    LinphoneCallLog *log = linphone_call_get_call_log(call);
    LinphoneAddress *toAddr = linphone_call_log_get_to_address(log);
    LinphoneAddress *fromAddr = linphone_call_log_get_from_address(log);
    
    NSString *toUserName = [XQLPOCBridge getUserNameWithAddress:toAddr];
    NSString *toDisplayName = [XQLPOCBridge getDisplayNameWithAddress:toAddr];
    
    NSString *fromUserName = [XQLPOCBridge getUserNameWithAddress:fromAddr];
    NSString *fromDisplayName = [XQLPOCBridge getDisplayNameWithAddress:fromAddr];
    
//    linphone_address_unref(toAddr);
//    linphone_address_unref(fromAddr);
//    linphone_call_log_unref(log);
    
	if (state == LinphoneCallIncomingReceived) {
        // 有系统通话存在, 挂断，返回在忙
		/*first step is to re-enable ctcall center*/
		CTCallCenter *lCTCallCenter = [[CTCallCenter alloc] init];
		/*should we reject this call ?*/
		if ([lCTCallCenter currentCalls] != nil) {
			char *tmp = linphone_call_get_remote_address_as_string(call);
			if (tmp) {
				LOGI(@"Mobile call ongoing... rejecting call from [%s]", tmp);
				ms_free(tmp);
			}
            
            linphone_call_decline(call, LinphoneReasonBusy);
			return;
		}
        
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            if(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max) {
                if (!incallBgTask) {
                    incallBgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
                        LOGW(@"Call cannot ring any more, too late");
                        [[UIApplication sharedApplication] endBackgroundTask:incallBgTask];
                        incallBgTask = 0;
                    }];
                    
                    if (data->timer) {
                        [[NSRunLoop currentRunLoop] addTimer:data->timer forMode:NSRunLoopCommonModes];
                    }
                }
            }
		}
        
        // 获取当前通话列表
        //const bctbx_list_t *callList = linphone_core_get_calls(LC);
	}
    
	// we keep the speaker auto-enabled state in this static so that we don't
	// force-enable it on ICE re-invite if the user disabled it.
	static BOOL speaker_already_enabled = FALSE;

	// Disable speaker when no more call
	if ((state == LinphoneCallEnd || state == LinphoneCallError)) {
		speaker_already_enabled = FALSE;
		if (linphone_core_get_calls_nb(theLinphoneCore) == 0) {
			[self setSpeakerEnabled:FALSE];
			[self removeCTCallCenterCb];
			// disable this because I don't find anygood reason for it: _bluetoothAvailable = FALSE;
			// furthermore it introduces a bug when calling multiple times since route may not be
			// reconfigured between cause leading to bluetooth being disabled while it should not
			_bluetoothEnabled = FALSE;
			/*IOS specific*/
			linphone_core_start_dtmf_stream(theLinphoneCore);
            if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max && ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)) {
                if (data->timer) {
                    [data->timer invalidate];
                    data->timer = nil;
                }
                
                linphone_core_set_network_reachable(LC, FALSE);
                LinphoneManager.instance.connectivity = none;
            }
		}
        
		if (incallBgTask) {
			[[UIApplication sharedApplication] endBackgroundTask:incallBgTask];
			incallBgTask = 0;
		}
        
		if (data != nil && data->notification != nil) {
			LinphoneCallLog *log = linphone_call_get_call_log(call);

			// cancel local notif if needed
			if (data->timer) {
				[data->timer invalidate];
				data->timer = nil;
			}
			[[UIApplication sharedApplication] cancelLocalNotification:data->notification];

			data->notification = nil;

			if (log == NULL || linphone_call_log_get_status(log) == LinphoneCallMissed) {
                LOGE(@"You missed a call from %@, to %@", fromUserName, toUserName);
			}
		}
	}
    
	if (state == LinphoneCallReleased) {
		if (data != NULL) {
			linphone_call_set_user_data(call, NULL);
			CFBridgingRelease((__bridge CFTypeRef)(data));
		}
	}
    
	// Enable speaker when video
	if (state == LinphoneCallIncomingReceived || state == LinphoneCallOutgoingInit || state == LinphoneCallConnected ||
		state == LinphoneCallStreamsRunning) {
		if (linphone_call_params_video_enabled(linphone_call_get_current_params(call)) && !speaker_already_enabled) {
            // 设置为扬声器
//			[self setSpeakerEnabled:TRUE];
//			speaker_already_enabled = TRUE;
		}
	}
    
	if (state == LinphoneCallConnected && !mCallCenter) {
		/*only register CT call center CB for connected call*/
        // 监听系统电话
		[self setupGSMInteraction];
	}
    
    NSString *messageStr = @"";
    if (message) {
        messageStr = [NSString stringWithCString:message encoding:NSUTF8StringEncoding];
    }
    
	NSDictionary *dict = @{
        @"call":[NSValue valueWithPointer:call],
		@"state" : [NSNumber numberWithInt:state],
		@"message" : messageStr,
        @"from":fromUserName,
        @"fromDisplayName":fromDisplayName,
        @"to":toUserName,
        @"toDisplayName":toDisplayName
	};
    
    LOG(@"\n\n\n call StateChanged = %@ \n\n\n", dict);
    [[NSNotificationCenter defaultCenter] postNotificationName:kLinphoneCallChange object:nil userInfo:dict];
}

static void linphone_iphone_call_state(LinphoneCore *lc, LinphoneCall *call, LinphoneCallState state,
									   const char *message) {
	[LinphoneManager.instance onCall:call StateChanged:state withMessage:message];
}

#pragma mark - Transfert State Functions

static void linphone_iphone_transfer_state_changed(LinphoneCore *lc, LinphoneCall *call, LinphoneCallState state) {
    LOGI(@"\n linphone_iphone_transfer_state_changed state = %d \n", state);
}

#pragma mark - Global state change

static void linphone_iphone_global_state_changed(LinphoneCore *lc, LinphoneGlobalState gstate, const char *message) {
	[LinphoneManager.instance onGlobalStateChanged:gstate withMessage:message];
}

- (void)onGlobalStateChanged:(LinphoneGlobalState)state withMessage:(const char *)message {
	LOG(@"\n\n onGlobalStateChanged: %d (message: %s) \n\n", state, message);
    
	NSDictionary *dict = [NSDictionary
		dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:state], @"state",
									 [NSString stringWithUTF8String:message ? message : ""], @"message", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLinphoneGlobalStateUpdate object:self userInfo:dict];
}

- (void)globalStateChangedNotificationHandler:(NSNotification *)notif {
	if ((LinphoneGlobalState)[[[notif userInfo] valueForKey:@"state"] integerValue] == LinphoneGlobalOn) {
		[self finishCoreConfiguration];
	}
}

#pragma mark - Configuring status changed

static void linphone_iphone_configuring_status_changed(LinphoneCore *lc, LinphoneConfiguringState status,
													   const char *message) {
	[LinphoneManager.instance onConfiguringStatusChanged:status withMessage:message];
}

- (void)onConfiguringStatusChanged:(LinphoneConfiguringState)status withMessage:(const char *)message {
	LOG(@"\n onConfiguringStatusChanged: %s %@ \n", linphone_configuring_state_to_string(status),
		 message ? [NSString stringWithFormat:@"(message: %s)", message] : @"");

	NSDictionary *dict = [NSDictionary
		dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:status], @"state",
									 [NSString stringWithUTF8String:message ? message : ""], @"message", nil];

	// dispatch the notification asynchronously
	dispatch_async(dispatch_get_main_queue(), ^(void) {
	  [NSNotificationCenter.defaultCenter postNotificationName:kLinphoneConfiguringStateUpdate
														object:self
													  userInfo:dict];
	});
}

#pragma mark - Registration State Functions

- (void)onRegister:(LinphoneCore *)lc
			   cfg:(LinphoneProxyConfig *)cfg
			 state:(LinphoneRegistrationState)state
		   message:(const char *)cmessage {
	LinphoneReason reason = linphone_proxy_config_get_error(cfg);
	NSString *message = nil;
	switch (reason) {
		case LinphoneReasonBadCredentials:
			message = NSLocalizedString(@"Bad credentials, check your account settings", nil);
			break;
		case LinphoneReasonNoResponse:
			message = NSLocalizedString(@"No response received from remote", nil);
			break;
		case LinphoneReasonUnsupportedContent:
			message = NSLocalizedString(@"Unsupported content", nil);
			break;
		case LinphoneReasonIOError:
			message = NSLocalizedString(
				@"Cannot reach the server: either it is an invalid address or it may be temporary down.", nil);
			break;

		case LinphoneReasonUnauthorized:
			message = NSLocalizedString(@"Operation is unauthorized because missing credential", nil);
			break;
		case LinphoneReasonNoMatch:
			message = NSLocalizedString(@"Operation could not be executed by server or remote client because it "
										@"didn't have any context for it",
										nil);
			break;
		case LinphoneReasonMovedPermanently:
			message = NSLocalizedString(@"Resource moved permanently", nil);
			break;
		case LinphoneReasonGone:
			message = NSLocalizedString(@"Resource no longer exists", nil);
			break;
		case LinphoneReasonTemporarilyUnavailable:
			message = NSLocalizedString(@"Temporarily unavailable", nil);
			break;
		case LinphoneReasonAddressIncomplete:
			message = NSLocalizedString(@"Address incomplete", nil);
			break;
		case LinphoneReasonNotImplemented:
			message = NSLocalizedString(@"Not implemented", nil);
			break;
		case LinphoneReasonBadGateway:
			message = NSLocalizedString(@"Bad gateway", nil);
			break;
		case LinphoneReasonServerTimeout:
			message = NSLocalizedString(@"Server timeout", nil);
			break;
		case LinphoneReasonNotAcceptable:
		case LinphoneReasonDoNotDisturb:
		case LinphoneReasonDeclined:
		case LinphoneReasonNotFound:
		case LinphoneReasonNotAnswered:
		case LinphoneReasonBusy:
		case LinphoneReasonNone:
		case LinphoneReasonUnknown:
			message = NSLocalizedString(@"Unknown error", nil);
			break;
	}
    
    const LinphoneAddress * address = linphone_proxy_config_get_identity_address(cfg);
    NSString *usernameStr = [XQLPOCBridge getUserNameWithAddress:address];
    NSString *domain = [XQLPOCBridge getDomainWithAddress:address];
    
    LOGI(@"message = %@", message);
    
    if (cmessage) {
        message = [NSString stringWithCString:cmessage encoding:NSUTF8StringEncoding];
    }
	
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:state], @"state",			[NSValue valueWithPointer:cfg], @"cfg", message, @"message", usernameStr, @"username", domain, @"domain", nil];
    
    LOG(@"\n\n\n ronRegistere = %@ \n\n\n", dict);
    
	[NSNotificationCenter.defaultCenter postNotificationName:kLinphoneRegistrationUpdate object:self userInfo:dict];
}

static void linphone_iphone_registration_state(LinphoneCore *lc, LinphoneProxyConfig *cfg,
											   LinphoneRegistrationState state, const char *message) {
	[LinphoneManager.instance onRegister:lc cfg:cfg state:state message:message];
}

#pragma mark - Auth info Function

static void linphone_authentication_requested(LinphoneCore *lc, LinphoneAuthInfo *info, LinphoneAuthMethod method) {
    LOGI(@"\n linphone_authentication_requested \n");
}

#pragma mark - Text Received Functions

- (void)onMessageReceived:(LinphoneCore *)lc room:(LinphoneChatRoom *)room message:(LinphoneChatMessage *)msg {
#pragma deploymate push "ignored-api-availability"
	
#pragma deploymate pop
    NSString *callID = [NSString stringWithUTF8String:linphone_chat_message_get_custom_header(msg, "Call-ID")];
    const LinphoneAddress *remoteAddress = linphone_chat_message_get_from_address(msg);
    const LinphoneAddress *localAddress = linphone_chat_message_get_to_address(msg);
    
    // 获取完整对方url
	char *c_address = linphone_address_as_string_uri_only(remoteAddress);
    NSString *remote_uri = @"";
    if (c_address) {
        remote_uri = [NSString stringWithUTF8String:c_address];
    }
	ms_free(c_address);
    
    // 获取对方username
    NSString *from = [XQLPOCBridge getUserNameWithAddress:remoteAddress];
    // 获取自己的username
    NSString *to = [XQLPOCBridge getUserNameWithAddress:localAddress];
    // 获取消息
    NSString *text = [XQLPOCBridge getMessageWithChatMessage:msg];
    // 获取对方的displayname
    NSString *fromDisplayNameStr = [XQLPOCBridge getDisplayNameWithAddress:remoteAddress];
    // 获取messageID
    NSString *messageID = [XQLPOCBridge getMessageIDWithChatMessage:msg];
    
    NSDictionary *dict = @{@"callID":callID, @"remoteURL":remote_uri, @"from":from, @"to":to, @"text":text, @"fromDisplayName":fromDisplayNameStr, @"messageID":messageID};
    
    LOG(@"\n\n\n onMessageReceived dict = %@ \n\n\n", dict);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLinphoneMessageReceived object:nil userInfo:dict];
}

static void linphone_iphone_message_received(LinphoneCore *lc, LinphoneChatRoom *room, LinphoneChatMessage *message) {
	[LinphoneManager.instance onMessageReceived:lc room:room message:message];
}

- (void)onNotifyReceived:(LinphoneCore *)lc
				   event:(LinphoneEvent *)lev
			 notifyEvent:(const char *)notified_event
				 content:(const LinphoneContent *)body {
	// Post event
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	[dict setObject:[NSValue valueWithPointer:lev] forKey:@"event"];
	[dict setObject:[NSString stringWithUTF8String:notified_event] forKey:@"notified_event"];
	if (body != NULL) {
		[dict setObject:[NSValue valueWithPointer:body] forKey:@"content"];
	}
    
    LOG(@"\n\n\n linphone_iphone_message_received dict = %@ \n\n\n", dict);
    
	[NSNotificationCenter.defaultCenter postNotificationName:kLinphoneNotifyReceived object:self userInfo:dict];
}

static void linphone_iphone_notify_received(LinphoneCore *lc, LinphoneEvent *lev, const char *notified_event,
											const LinphoneContent *body) {
	[LinphoneManager.instance onNotifyReceived:lc
																			event:lev
																	  notifyEvent:notified_event
																		  content:body];
}

- (void)onNotifyPresenceReceivedForUriOrTel:(LinphoneCore *)lc
									 friend:(LinphoneFriend *)lf
										uri:(const char *)uri
							  presenceModel:(const LinphonePresenceModel *)model {
	// Post event
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	[dict setObject:[NSValue valueWithPointer:lf] forKey:@"friend"];
	[dict setObject:[NSValue valueWithPointer:uri] forKey:@"uri"];
	[dict setObject:[NSValue valueWithPointer:model] forKey:@"presence_model"];
    
    LOG(@"\n onNotifyPresenceReceivedForUriOrTel dict = %@ \n", dict);
    
//	[NSNotificationCenter.defaultCenter postNotificationName:kLinphoneNotifyPresenceReceivedForUriOrTel object:self userInfo:dict];
}

static void linphone_iphone_notify_presence_received_for_uri_or_tel(LinphoneCore *lc, LinphoneFriend *lf,
																	const char *uri_or_tel,
																	const LinphonePresenceModel *presence_model) {
	[LinphoneManager.instance onNotifyPresenceReceivedForUriOrTel:lc friend:lf uri:uri_or_tel presenceModel:presence_model];
    
}

static void linphone_iphone_call_encryption_changed(LinphoneCore *lc, LinphoneCall *call, bool_t on,
													const char *authentication_token) {
	[LinphoneManager.instance onCallEncryptionChanged:lc
																					call:call
																					  on:on
																				   token:authentication_token];
}

- (void)onCallEncryptionChanged:(LinphoneCore *)lc
						   call:(LinphoneCall *)call
							 on:(BOOL)on
						  token:(const char *)authentication_token {
	// Post event
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	[dict setObject:[NSValue valueWithPointer:call] forKey:@"call"];
	[dict setObject:[NSNumber numberWithBool:on] forKey:@"on"];
	[dict setObject:[NSString stringWithUTF8String:authentication_token] forKey:@"token"];
    
    LOG(@"\n onCallEncryptionChanged dict = %@ \n", dict);
    
//	[NSNotificationCenter.defaultCenter postNotificationName:kLinphoneCallEncryptionChanged object:self userInfo:dict];
}

#pragma mark - Message composition start
// 正在编辑消息
- (void)onMessageComposeReceived:(LinphoneCore *)core forRoom:(LinphoneChatRoom *)room {
//    const LinphoneAddress *address = linphone_chat_room_get_peer_address(room);
//    NSLog(@"address = %@", linphone_address_get_username(address));
	[NSNotificationCenter.defaultCenter postNotificationName:kLinphoneTextComposeEvent
													  object:self
													userInfo:@{
														@"room" : [NSValue valueWithPointer:room]
													}];
}

static void linphone_iphone_is_composing_received(LinphoneCore *lc, LinphoneChatRoom *room) {
	[LinphoneManager.instance onMessageComposeReceived:lc forRoom:room];
}

#pragma mark - Network Functions

- (SCNetworkReachabilityRef)getProxyReachability {
	return proxyReachability;
}

+ (void)kickOffNetworkConnection {
	static BOOL in_progress = FALSE;
	if (in_progress) {
		LOGW(@"Connection kickoff already in progress");
		return;
	}
	in_progress = TRUE;
	/* start a new thread to avoid blocking the main ui in case of peer host failure */
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
	  static int sleep_us = 10000;
	  static int timeout_s = 5;
	  BOOL timeout_reached = FALSE;
	  int loop = 0;
	  CFWriteStreamRef writeStream;
	  CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef) @"192.168.0.200" /*"linphone.org"*/, 15000, nil,
										 &writeStream);
	  BOOL res = CFWriteStreamOpen(writeStream);
	  const char *buff = "hello";
	  time_t start = time(NULL);
	  time_t loop_time;

	  if (res == FALSE) {
		  LOGI(@"Could not open write stream, backing off");
		  CFRelease(writeStream);
		  in_progress = FALSE;
		  return;
	  }

	  // check stream status and handle timeout
	  CFStreamStatus status = CFWriteStreamGetStatus(writeStream);
	  while (status != kCFStreamStatusOpen && status != kCFStreamStatusError) {
		  usleep(sleep_us);
		  status = CFWriteStreamGetStatus(writeStream);
		  loop_time = time(NULL);
		  if (loop_time - start >= timeout_s) {
			  timeout_reached = TRUE;
			  break;
		  }
		  loop++;
	  }

	  if (status == kCFStreamStatusOpen) {
		  CFWriteStreamWrite(writeStream, (const UInt8 *)buff, strlen(buff));
	  } else if (!timeout_reached) {
		  CFErrorRef error = CFWriteStreamCopyError(writeStream);
		  LOGD(@"CFStreamError: %@", error);
		  CFRelease(error);
	  } else if (timeout_reached) {
		  LOGI(@"CFStream timeout reached");
	  }
	  CFWriteStreamClose(writeStream);
	  CFRelease(writeStream);
	  in_progress = FALSE;
	});
}

+ (NSString *)getCurrentWifiSSID {
#if TARGET_IPHONE_SIMULATOR
	return @"Sim_err_SSID_NotSupported";
#else
	NSString *data = nil;
	CFDictionaryRef dict = CNCopyCurrentNetworkInfo((CFStringRef) @"en0");
	if (dict) {
		LOGI(@"AP Wifi: %@", dict);
		data = [NSString stringWithString:(NSString *)CFDictionaryGetValue(dict, @"SSID")];
		CFRelease(dict);
	}
	return data;
#endif
}

static void showNetworkFlags(SCNetworkReachabilityFlags flags) {
	NSMutableString *log = [[NSMutableString alloc] initWithString:@"Network connection flags: "];
	if (flags == 0)
		[log appendString:@"no flags."];
	if (flags & kSCNetworkReachabilityFlagsTransientConnection)
		[log appendString:@"kSCNetworkReachabilityFlagsTransientConnection, "];
	if (flags & kSCNetworkReachabilityFlagsReachable)
		[log appendString:@"kSCNetworkReachabilityFlagsReachable, "];
	if (flags & kSCNetworkReachabilityFlagsConnectionRequired)
		[log appendString:@"kSCNetworkReachabilityFlagsConnectionRequired, "];
	if (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic)
		[log appendString:@"kSCNetworkReachabilityFlagsConnectionOnTraffic, "];
	if (flags & kSCNetworkReachabilityFlagsConnectionOnDemand)
		[log appendString:@"kSCNetworkReachabilityFlagsConnectionOnDemand, "];
	if (flags & kSCNetworkReachabilityFlagsIsLocalAddress)
		[log appendString:@"kSCNetworkReachabilityFlagsIsLocalAddress, "];
	if (flags & kSCNetworkReachabilityFlagsIsDirect)
		[log appendString:@"kSCNetworkReachabilityFlagsIsDirect, "];
	if (flags & kSCNetworkReachabilityFlagsIsWWAN)
		[log appendString:@"kSCNetworkReachabilityFlagsIsWWAN, "];
	LOGI(@"%@", log);
}

//This callback keeps tracks of wifi SSID changes.
static void networkReachabilityNotification(CFNotificationCenterRef center, void *observer, CFStringRef name,
											const void *object, CFDictionaryRef userInfo) {
	LinphoneManager *mgr = LinphoneManager.instance;
	SCNetworkReachabilityFlags flags;

	// for an unknown reason, we are receiving multiple time the notification, so
	// we will skip each time the SSID did not change
	NSString *newSSID = [LinphoneManager getCurrentWifiSSID];
	if ([newSSID compare:mgr.SSID] == NSOrderedSame)
		return;

	
	if (newSSID != Nil && newSSID.length > 0 && mgr.SSID != Nil && newSSID.length > 0){
		if (SCNetworkReachabilityGetFlags([mgr getProxyReachability], &flags)) {
			LOGI(@"Wifi SSID changed, resesting transports.");
			mgr.connectivity=none; //this will trigger a connectivity change in networkReachabilityCallback.
			networkReachabilityCallBack([mgr getProxyReachability], flags, nil);
		}
	}
	mgr.SSID = newSSID;

	
}

void networkReachabilityCallBack(SCNetworkReachabilityRef target, SCNetworkReachabilityFlags flags, void *nilCtx) {
	showNetworkFlags(flags);
	LinphoneManager *lm = LinphoneManager.instance;
	SCNetworkReachabilityFlags networkDownFlags = kSCNetworkReachabilityFlagsConnectionRequired |
												  kSCNetworkReachabilityFlagsConnectionOnTraffic |
												  kSCNetworkReachabilityFlagsConnectionOnDemand;

	if (theLinphoneCore != nil) {
		LinphoneProxyConfig *proxy = linphone_core_get_default_proxy_config(theLinphoneCore);

		struct NetworkReachabilityContext *ctx = nilCtx ? ((struct NetworkReachabilityContext *)nilCtx) : 0;
		if ((flags == 0) || (flags & networkDownFlags)) {
			linphone_core_set_network_reachable(theLinphoneCore, false);
			lm.connectivity = none;
			[LinphoneManager kickOffNetworkConnection];
		} else {
			Connectivity newConnectivity;
			BOOL isWifiOnly = [lm lpConfigBoolForKey:@"wifi_only_preference" withDefault:FALSE];
			if (!ctx || ctx->testWWan)
				newConnectivity = flags & kSCNetworkReachabilityFlagsIsWWAN ? wwan : wifi;
			else
				newConnectivity = wifi;

			if (newConnectivity == wwan && proxy && isWifiOnly &&
				(lm.connectivity == newConnectivity || lm.connectivity == none)) {
				linphone_proxy_config_expires(proxy, 0);
			} else if (proxy) {
				NSInteger defaultExpire = [lm lpConfigIntForKey:@"default_expires"];
				if (defaultExpire >= 0)
					linphone_proxy_config_expires(proxy, (int)defaultExpire);
				// else keep default value from linphonecore
			}

			if (lm.connectivity != newConnectivity) {
				// connectivity has changed
				linphone_core_set_network_reachable(theLinphoneCore, false);
				if (newConnectivity == wwan && proxy && isWifiOnly) {
					linphone_proxy_config_expires(proxy, 0);
				}
				linphone_core_set_network_reachable(theLinphoneCore, true);
				linphone_core_iterate(theLinphoneCore);
				LOGI(@"Network connectivity changed to type [%s]", (newConnectivity == wifi ? "wifi" : "wwan"));
				lm.connectivity = newConnectivity;
			}
		}
		if (ctx && ctx->networkStateChanged) {
			(*ctx->networkStateChanged)(lm.connectivity);
		}
	}
}

- (void)setupNetworkReachabilityCallback {
	SCNetworkReachabilityContext *ctx = NULL;
	// any internet cnx
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;

	if (proxyReachability) {
		LOGI(@"Cancelling old network reachability");
		SCNetworkReachabilityUnscheduleFromRunLoop(proxyReachability, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
		CFRelease(proxyReachability);
		proxyReachability = nil;
	}

	// This notification is used to detect SSID change (switch of Wifi network). The ReachabilityCallback is
	// not triggered when switching between 2 private Wifi...
	// Since we cannot be sure we were already observer, remove ourself each time... to be improved
	_SSID = [LinphoneManager getCurrentWifiSSID];
	CFNotificationCenterRemoveObserver(CFNotificationCenterGetDarwinNotifyCenter(), (__bridge const void *)(self),
									   CFSTR("com.apple.system.config.network_change"), NULL);
	CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), (__bridge const void *)(self),
									networkReachabilityNotification, CFSTR("com.apple.system.config.network_change"),
									NULL, CFNotificationSuspensionBehaviorDeliverImmediately);

	proxyReachability =
		SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr *)&zeroAddress);

	if (!SCNetworkReachabilitySetCallback(proxyReachability, (SCNetworkReachabilityCallBack)networkReachabilityCallBack,
										  ctx)) {
		LOGE(@"Cannot register reachability cb: %s", SCErrorString(SCError()));
		return;
	}
	if (!SCNetworkReachabilityScheduleWithRunLoop(proxyReachability, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode)) {
		LOGE(@"Cannot register schedule reachability cb: %s", SCErrorString(SCError()));
		return;
	}

	// this check is to know network connectivity right now without waiting for a change. Don'nt remove it unless you
	// have good reason. Jehan
	SCNetworkReachabilityFlags flags;
	if (SCNetworkReachabilityGetFlags(proxyReachability, &flags)) {
		networkReachabilityCallBack(proxyReachability, flags, nil);
	}
}

- (NetworkType)network {
#pragma deploymate push "ignored-api-availability"
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    NSString *currentRadio = info.currentRadioAccessTechnology;
    if ([currentRadio isEqualToString:CTRadioAccessTechnologyEdge]) {
        return network_2g;
    } else if ([currentRadio isEqualToString:CTRadioAccessTechnologyLTE]) {
        return network_4g;
    }
#pragma deploymate pop
    return network_3g;
}

#pragma mark -- call_stats_updated 状态更新

static void linphone_call_stats_updated(LinphoneCore *lc, LinphoneCall *call, const LinphoneCallStats *stats) {
    LinphoneStreamType type = linphone_call_stats_get_type(stats);
    
    LinphoneCallLog *log = linphone_call_get_call_log(call);
    LinphoneAddress *toAddr = linphone_call_log_get_to_address(log);
    LinphoneAddress *fromAddr = linphone_call_log_get_from_address(log);
    
    NSString *toUserName = [XQLPOCBridge getUserNameWithAddress:toAddr];
    
    NSString *fromUserName = [XQLPOCBridge getUserNameWithAddress:fromAddr];
    NSString *fromDisplayName = [XQLPOCBridge getDisplayNameWithAddress:fromAddr];
    
    NSDictionary *dict = @{
                           @"state" : [NSNumber numberWithInt:type],
                           @"from":fromUserName,
                           @"fromDisplayName":fromDisplayName,
                           @"to":toUserName,
                           };
    
    LOG(@"\n\n\n linphone_call_stats_updated dict = %@ \n\n\n", dict);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLinphoneCallUpdate object:nil userInfo:dict];
}

#pragma mark - ********* VTable 旧版函数回调 ******* 


static LinphoneCoreVTable linphonec_vtable = {
	.call_state_changed = (LinphoneCoreCallStateChangedCb)linphone_iphone_call_state,
	.registration_state_changed = linphone_iphone_registration_state,
	.notify_presence_received_for_uri_or_tel = linphone_iphone_notify_presence_received_for_uri_or_tel,
	.authentication_requested = linphone_authentication_requested,
	.message_received = linphone_iphone_message_received,
	.transfer_state_changed = linphone_iphone_transfer_state_changed,
	.is_composing_received = linphone_iphone_is_composing_received,
	.configuring_status = linphone_iphone_configuring_status_changed,
	.global_state_changed = linphone_iphone_global_state_changed,
	.notify_received = linphone_iphone_notify_received,
	.call_encryption_changed = linphone_iphone_call_encryption_changed,
    .call_stats_updated = linphone_call_stats_updated,
};


#pragma mark - 心跳

// scheduling loop
- (void)iterate {
    linphone_core_iterate(theLinphoneCore);
}

- (void)audioSessionInterrupted:(NSNotification *)notification {
	int interruptionType = [notification.userInfo[AVAudioSessionInterruptionTypeKey] intValue];
	if (interruptionType == AVAudioSessionInterruptionTypeBegan) {
		[self beginInterruption];
	} else if (interruptionType == AVAudioSessionInterruptionTypeEnded) {
		[self endInterruption];
	}
}

/** Should be called once per linphone_core_new() */
- (void)finishCoreConfiguration {
	// get default config from bundle
	NSString *zrtpSecretsFileName = [LinphoneManager documentFile:@"zrtp_secrets"];
	NSString *chatDBFileName = [LinphoneManager documentFile:kLinphoneInternalChatDBFilename];

	NSString *device = [[NSMutableString alloc]
		initWithString:[NSString
						   stringWithFormat:@"%@_%@_iOS%@",
											[NSBundle.mainBundle objectForInfoDictionaryKey:@"CFBundleDisplayName"],
											[LinphoneManager deviceModelIdentifier],
											UIDevice.currentDevice.systemVersion]];
	device = [device stringByReplacingOccurrencesOfString:@"," withString:@"."];
	device = [device stringByReplacingOccurrencesOfString:@" " withString:@"."];
	linphone_core_set_user_agent(theLinphoneCore, device.UTF8String, LINPHONE_IOS_VERSION);
    
	_contactSipField = [self lpConfigStringForKey:@"contact_im_type_value" withDefault:@"SIP"];
    
    // 设置音视频加密、聊天信息、通话打印信息路径
	linphone_core_set_zrtp_secrets_file(theLinphoneCore, [zrtpSecretsFileName UTF8String]);
	linphone_core_set_chat_database_path(theLinphoneCore, [chatDBFileName UTF8String]);
	linphone_core_set_call_logs_database_path(theLinphoneCore, [chatDBFileName UTF8String]);
    
    // 监测网络
	[self setupNetworkReachabilityCallback];
    
    // 设置static_picture路径
	NSString *path = [LinphoneManager bundleFile:@"nowebcamCIF.jpg"];
	if (path) {
		const char *imagePath = [path UTF8String];
		LOGI(@"Using '%s' as source image for no webcam", imagePath);
		linphone_core_set_static_picture(theLinphoneCore, imagePath);
	}
    
    // 设置前后摄像头设备
	/*DETECT cameras*/
	_frontCamId = _backCamId = nil;
	char **camlist = (char **)linphone_core_get_video_devices(theLinphoneCore);
    // 是否存在摄像设备
	if (camlist) {
		for (char *cam = *camlist; *camlist != NULL; cam = *++camlist) {
			if (strcmp(FRONT_CAM_NAME, cam) == 0) {
				_frontCamId = cam;
				// great set default cam to front
				LOGI(@"Setting default camera [%s]", _frontCamId);
				linphone_core_set_video_device(theLinphoneCore, _frontCamId);
			}
            
			if (strcmp(BACK_CAM_NAME, cam) == 0) {
				_backCamId = cam;
			}
		}
        
	}else {
		LOGW(@"No camera detected!");
        
	}
    
    // 是3G网络
	if (![LinphoneManager isNotIphone3G]) {
		LinphonePayloadType *pt = linphone_core_get_payload_type(theLinphoneCore, "SILK", 24000, -1);
		if (pt) {
			linphone_payload_type_enable(pt, FALSE);
			LOGW(@"SILK/24000 and video disabled on old iPhone 3G");
		}
        // 关闭视频显示, 和视频捕抓
		linphone_core_enable_video_display(theLinphoneCore, FALSE);
		linphone_core_enable_video_capture(theLinphoneCore, FALSE);
	}
	
	[self enableProxyPublish:([UIApplication sharedApplication].applicationState == UIApplicationStateActive)];
    
	LOG(@"Linphone [%s]  started on [%s]", linphone_core_get_version(), [[UIDevice currentDevice].model UTF8String]);

	// Post event
//	NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSValue valueWithPointer:theLinphoneCore] forKey:@"core"];
//	[NSNotificationCenter.defaultCenter postNotificationName:kLinphoneCoreUpdate object:LinphoneManager.instance userInfo:dict];
}

static BOOL libStarted = FALSE;

- (void)startLinphoneCore {
	if (libStarted) {
		LOGE(@"Liblinphone is already initialized!");
		return;
	}
    
	libStarted = TRUE;

	connectivity = none;
	signal(SIGPIPE, SIG_IGN);

	// create linphone core
	[self createLinphoneCore];
    
	// - Security fix - remove multi transport migration, because it enables tcp or udp, if by factoring settings only
	// tls is enabled. 	This is a problem for new installations.
    linphone_core_migrate_to_multi_transport(theLinphoneCore);
    
	// init audio session (just getting the instance will init)
	AVAudioSession *audioSession = [AVAudioSession sharedInstance];
	BOOL bAudioInputAvailable = audioSession.inputAvailable;
	NSError *err;
    
	if (![audioSession setActive:NO error:&err] && err) {
		LOGE(@"audioSession setActive failed: %@", [err description]);
	}
    
	if (!bAudioInputAvailable) {
        LOGI(@"No microphone.  You need to plug a microphone to your device to use the application.");
	}
    
    // 允许im通知
    LinphoneImNotifPolicy *im_notif_policy = linphone_core_get_im_notif_policy(theLinphoneCore);
    linphone_im_notif_policy_enable_all(im_notif_policy);
    
    // 是否在后台, 进入后台模式
	if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
		// go directly to bg mode
		[self enterBackgroundMode];
	}
}

void popup_link_account_cb(LinphoneAccountCreator *creator, LinphoneAccountCreatorStatus status, const char *resp) {
	if (status == LinphoneAccountCreatorStatusAccountExistWithAlias) {
		[LinphoneManager.instance lpConfigSetInt:0 forKey:@"must_link_account_time"];
	} else {
		LinphoneProxyConfig *cfg = linphone_core_get_default_proxy_config(LC);
		if (cfg) {
			UIAlertController *errView = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Link your account", nil)
																			 message:[NSString stringWithFormat:NSLocalizedString(@"Link your Linphone.org account %s to your phone number.", nil),
																					  linphone_address_get_username(linphone_proxy_config_get_identity_address(cfg))]
																	  preferredStyle:UIAlertControllerStyleAlert];
			
			UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Maybe later", nil)
																	style:UIAlertActionStyleDefault
																  handler:^(UIAlertAction * action) {}];
			
			UIAlertAction* continueAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Let's go", nil)
																	 style:UIAlertActionStyleDefault
																   handler:^(UIAlertAction * action) {
//																	   [PhoneMainView.instance changeCurrentView:AssistantLinkView.compositeViewDescription];
																   }];
			defaultAction.accessibilityLabel = @"Later";
			[errView addAction:defaultAction];
			[errView addAction:continueAction];
//			[PhoneMainView.instance presentViewController:errView animated:YES completion:nil];

			[LinphoneManager.instance
				lpConfigSetInt:[[NSDate date] dateByAddingTimeInterval:[LinphoneManager.instance
																		   lpConfigIntForKey:@"link_account_popup_time"
																				 withDefault:84200]]
								   .timeIntervalSince1970
						forKey:@"must_link_account_time"];
		}
	}
}

- (void)shouldPresentLinkPopup {
	NSDate *nextTime =
		[NSDate dateWithTimeIntervalSince1970:[self lpConfigIntForKey:@"must_link_account_time" withDefault:1]];
	NSDate *now = [NSDate date];
	if (nextTime.timeIntervalSince1970 > 0 && [now earlierDate:nextTime] == nextTime) {
		LinphoneProxyConfig *cfg = linphone_core_get_default_proxy_config(LC);
		if (cfg) {
			const char *username = linphone_address_get_username(linphone_proxy_config_get_identity_address(cfg));
			LinphoneAccountCreator *account_creator = linphone_account_creator_new(
				LC,
				[LinphoneManager.instance lpConfigStringForKey:@"xmlrpc_url" inSection:@"assistant" withDefault:@""]
					.UTF8String);
			linphone_account_creator_set_user_data(account_creator, (__bridge void *)(self));
			linphone_account_creator_cbs_set_is_account_linked(linphone_account_creator_get_callbacks(account_creator),
															 popup_link_account_cb);
			linphone_account_creator_set_username(account_creator, username);
			linphone_account_creator_is_account_linked(account_creator);
		}
	}
}

- (void)createLinphoneCore {
	// [self migrationAllPre];
	if (theLinphoneCore != nil) {
		LOGI(@"linphonecore is already created");
		return;
	}
//	[Log enableLogs:[self lpConfigIntForKey:@"debugenable_preference"]];
    // 设置log模式，默认开启
    [Log enableLogs:ORTP_DEBUG];
	connectivity = none;
    
    /*
     // 设置铃声
     NSString *ring =
     ([LinphoneManager bundleFile:[self lpConfigStringForKey:@"local_ring" inSection:@"sound"].lastPathComponent]
     ?: [LinphoneManager bundleFile:@"notes_of_the_optimistic.caf"])
     .lastPathComponent;
     NSString *ringback =
     ([LinphoneManager bundleFile:[self lpConfigStringForKey:@"remote_ring" inSection:@"sound"].lastPathComponent]
     ?: [LinphoneManager bundleFile:@"ringback.wav"])
     .lastPathComponent;
     NSString *hold =
     ([LinphoneManager bundleFile:[self lpConfigStringForKey:@"hold_music" inSection:@"sound"].lastPathComponent]
     ?: [LinphoneManager bundleFile:@"hold.mkv"])
     .lastPathComponent;
     [self lpConfigSetString:[LinphoneManager bundleFile:ring] forKey:@"local_ring" inSection:@"sound"];
     [self lpConfigSetString:[LinphoneManager bundleFile:ringback] forKey:@"remote_ring" inSection:@"sound"];
     [self lpConfigSetString:[LinphoneManager bundleFile:hold] forKey:@"hold_music" inSection:@"sound"];
     */
    
    // 设置日志打印级别
    [Log enableLogs:self.logLevel];
    
    // 旧版创建core
	//theLinphoneCore = linphone_core_new_with_config(&linphonec_vtable, _configDb, (__bridge void *)(self));
    
    // 新版创建core
    LinphoneFactory *factory = linphone_factory_get();
    // 初始化监听回调
    [self initCbs:factory];
    // 初始化core
    theLinphoneCore = linphone_factory_create_core_with_config(factory, self.cbs, _configDb);
    
    // 加载插件
	// Load plugins if available in the linphone SDK - otherwise these calls will do nothing
	MSFactory *f = linphone_core_get_ms_factory(theLinphoneCore);
	libmssilk_init(f);
	libmsamr_init(f);
	libmsx264_init(f);
	libmsopenh264_init(f);
	//libmsbcg729_init(f);
	libmswebrtc_init(f);
    
    linphone_core_reload_ms_plugins(theLinphoneCore, NULL);
    
    // 删除以前账号信息，不让自动登录
    [self removeAllAccounts];
    
    /* set the CA file no matter what, since the remote provisioning could be hitting an HTTPS server */
    //linphone_core_set_root_ca(theLinphoneCore, [LinphoneManager bundleFile:@"rootca.pem"].UTF8String);
    //linphone_core_set_user_certificates_path(theLinphoneCore, [LinphoneManager cacheDirectory].UTF8String);
    
    // 迁移资料
	//[self migrationAllPost];
    
    /* The core will call the linphone_iphone_configuring_status_changed callback when the remote provisioning is loaded
     (or skipped).
     Wait for this to finish the code configuration */
    
    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(audioSessionInterrupted:)
                                               name:AVAudioSessionInterruptionNotification
                                             object:nil];
    
    //[NSNotificationCenter.defaultCenter addObserver:self selector:@selector(globalStateChangedNotificationHandler:) name:kLinphoneGlobalStateUpdate object:nil];
    
	/* set the CA file no matter what, since the remote provisioning could be hitting an HTTPS server */
//	linphone_core_set_root_ca(theLinphoneCore, [LinphoneManager bundleFile:@"rootca.pem"].UTF8String);
//	linphone_core_set_user_certificates_path(theLinphoneCore, [LinphoneManager cacheDirectory].UTF8String);
    
    
	/*call iterate once immediately in order to initiate background connections with sip server or remote provisioning
	 * grab, if any */
	linphone_core_iterate(theLinphoneCore);
	// start scheduler 心跳
	mIterateTimer =
		[NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(iterate) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:mIterateTimer forMode:NSRunLoopCommonModes];
}

// 初始化监听
- (void)initCbs:(LinphoneFactory *)factory {
    if (self.cbs) {
        LOGW(@"cbs already exist");
        return;
    }
    
    self.cbs = linphone_factory_create_core_cbs(factory);
    if (!self.cbs) {
        LOGE(@"cbs not exist");
        return;
    }
    
//    .notify_presence_received_for_uri_or_tel = linphone_iphone_notify_presence_received_for_uri_or_tel,
//    .is_composing_received = linphone_iphone_is_composing_received,
//    .configuring_status = linphone_iphone_configuring_status_changed,
//    .notify_received = linphone_iphone_notify_received,
    
    // 设置cbs监听
    linphone_core_cbs_set_global_state_changed(self.cbs, linphone_iphone_global_state_changed);
    linphone_core_cbs_set_authentication_requested(self.cbs, linphone_authentication_requested);
    linphone_core_cbs_set_registration_state_changed(self.cbs, linphone_iphone_registration_state);
    linphone_core_cbs_set_call_state_changed(self.cbs, linphone_iphone_call_state);
    linphone_core_cbs_set_call_stats_updated(self.cbs, linphone_call_stats_updated);
    linphone_core_cbs_set_message_received(self.cbs, linphone_iphone_message_received);
    
    
    // 呼叫相关的回调, 配这个函数使用linphone_call_add_callbacks(call, cbs)
    /*
    if (self.callCbs) {
        LOGW(@"callCbs already exist");
        return;
    }
    
    self.callCbs = linphone_factory_create_call_cbs(factory);
    if (!self.callCbs) {
        LOGE(@"callCbs not exist");
        return;
    }
    
    linphone_call_cbs_set_info_message_received(self.callCbs, linphone_call_info_message_received);
    linphone_call_cbs_set_state_changed(self.callCbs, linphone_call_state_changed);
    linphone_call_cbs_set_stats_updated(self.callCbs, linphone_call_stats_update);
    linphone_call_cbs_set_encryption_changed(self.callCbs, linphone_call_encryption_changed);
    linphone_call_cbs_set_transfer_state_changed(self.callCbs, linphone_call_transfer_state_changed);
     */
    
    // 发送消息
    /*
    LinphoneInfoMessage *im = linphone_core_create_info_message(LC);
    LinphoneContent *content = linphone_core_create_content(LC);
    linphone_content_set_string_buffer(content, "aaaa");
    linphone_info_message_set_content(im, content);
    LinphoneCall *call = linphone_core_get_current_call(LC);
    linphone_call_send_info_message(call, im);
     */
}

#pragma mark -- linphone_call_cbs_set_info_message_received

static void linphone_call_info_message_received(LinphoneCall *call, const LinphoneInfoMessage *msg) {
    LOG(@"\n\n\n linphone_call_info_message_received \n\n\n");
}

#pragma mark -- linphone_call_cbs_set_state_changed

static void linphone_call_state_changed(LinphoneCall *call, LinphoneCallState cstate, const char *message) {
    LOG(@"\n\n\n linphone_call_state_changed state = %d, message = %s \n\n\n", cstate, message);
}

#pragma mark -- linphone_call_cbs_set_stats_updated

static void linphone_call_stats_update(LinphoneCall *call, const LinphoneCallStats *stats) {
    LOG(@"\n\n\n linphone_call_stats_update \n\n\n");
}

#pragma mark -- linphone_call_cbs_set_encryption_changed

static void linphone_call_encryption_changed(LinphoneCall *call, bool_t on, const char *authentication_token) {
    LOG(@"\n\n\n linphone_call_encryption_changed \n\n\n");
}

#pragma mark -- linphone_call_cbs_set_transfer_state_changed

static void linphone_call_transfer_state_changed(LinphoneCall *call, LinphoneCallState cstate) {
    LOG(@"\n\n\n linphone_call_transfer_state_changed \n\n\n");
}

- (void)destroyLinphoneCore {
	[mIterateTimer invalidate];
	// just in case
	[self removeCTCallCenterCb];

	if (theLinphoneCore != nil) { // just in case application terminate before linphone core initialization
		linphone_core_unref(theLinphoneCore);
		LOGI(@"Destroy linphonecore %p", theLinphoneCore);
		theLinphoneCore = nil;

		// Post event
		NSDictionary *dict =
			[NSDictionary dictionaryWithObject:[NSValue valueWithPointer:theLinphoneCore] forKey:@"core"];
		[NSNotificationCenter.defaultCenter postNotificationName:kLinphoneCoreUpdate
														  object:LinphoneManager.instance
														userInfo:dict];

		SCNetworkReachabilityUnscheduleFromRunLoop(proxyReachability, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
		if (proxyReachability)
			CFRelease(proxyReachability);
		proxyReachability = nil;
	}
	libStarted = FALSE;
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)resetLinphoneCore {
	[self destroyLinphoneCore];
	[self createLinphoneCore];
	
	// reset network state to trigger a new network connectivity assessment
	linphone_core_set_network_reachable(theLinphoneCore, FALSE);
}

static int comp_call_id(const LinphoneCall *call, const char *callid) {
	if (linphone_call_log_get_call_id(linphone_call_get_call_log(call)) == nil) {
		ms_error("no callid for call [%p]", call);
		return 1;
	}
	return strcmp(linphone_call_log_get_call_id(linphone_call_get_call_log(call)), callid);
}

- (void)cancelLocalNotifTimerForCallId:(NSString *)callid {
	// first, make sure this callid is not already involved in a call
	const bctbx_list_t *calls = linphone_core_get_calls(theLinphoneCore);
	bctbx_list_t *call = bctbx_list_find_custom(calls, (bctbx_compare_func)comp_call_id, [callid UTF8String]);
	if (call != NULL) {
		LinphoneCallAppData *data =
			(__bridge LinphoneCallAppData *)(linphone_call_get_user_data((LinphoneCall *)call->data));
		if (data->timer)
			[data->timer invalidate];
		data->timer = nil;
		return;
	}
}

- (void)acceptCallForCallId:(NSString *)callid {
	// first, make sure this callid is not already involved in a call
	const bctbx_list_t *calls = linphone_core_get_calls(theLinphoneCore);
	bctbx_list_t *call = bctbx_list_find_custom(calls, (bctbx_compare_func)comp_call_id, [callid UTF8String]);
	if (call != NULL) {
		const LinphoneVideoPolicy *video_policy = linphone_core_get_video_policy(theLinphoneCore);
		bool with_video = video_policy->automatically_accept;
		[self acceptCall:(LinphoneCall *)call->data evenWithVideo:with_video];
	};
}

- (BOOL)resignActive {
	linphone_core_stop_dtmf_stream(theLinphoneCore);
	return YES;
}

- (void)playMessageSound {
	AudioServicesPlaySystemSound(LinphoneManager.instance.sounds.vibrate);
}

static int comp_call_state_paused(const LinphoneCall *call, const void *param) {
	return linphone_call_get_state(call) != LinphoneCallPaused;
}

- (void)startCallPausedLongRunningTask {
	pausedCallBgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
	  LOGW(@"Call cannot be paused any more, too late");
	  [[UIApplication sharedApplication] endBackgroundTask:pausedCallBgTask];
	}];
	LOGI(@"Long running task started, remaining [%g s] because at least one call is paused",
		 [[UIApplication sharedApplication] backgroundTimeRemaining]);
}

- (void)enableProxyPublish:(BOOL)enabled {
	if (linphone_core_get_global_state(LC) != LinphoneGlobalOn || !linphone_core_get_default_friend_list(LC)) {
		LOGW(@"Not changing presence configuration because linphone core not ready yet");
		return;
	}	

	if ([self lpConfigBoolForKey:@"publish_presence"]) {
		// set present to "tv", because "available" does not work yet
		if (enabled) {
			linphone_core_set_presence_model(
											 LC, linphone_core_create_presence_model_with_activity(LC, LinphonePresenceActivityTV, NULL));
		}

		const MSList *proxies = linphone_core_get_proxy_config_list(LC);
		while (proxies) {
			LinphoneProxyConfig *cfg = proxies->data;
			linphone_proxy_config_edit(cfg);
			linphone_proxy_config_enable_publish(cfg, enabled);
			linphone_proxy_config_done(cfg);
			proxies = proxies->next;
		}
		// force registration update first, then update friend list subscription
		linphone_core_iterate(theLinphoneCore);
	}

	linphone_friend_list_enable_subscriptions(linphone_core_get_default_friend_list(LC), enabled);
}

- (BOOL)enterBackgroundMode {
	LinphoneProxyConfig *proxyCfg = linphone_core_get_default_proxy_config(theLinphoneCore);
	BOOL shouldEnterBgMode = FALSE;

	// disable presence
	[self enableProxyPublish:NO];
	
	// handle proxy config if any
	if (proxyCfg) {
		const char *refkey = proxyCfg ? linphone_proxy_config_get_ref_key(proxyCfg) : NULL;
		BOOL pushNotifEnabled = (refkey && strcmp(refkey, "push_notification") == 0);
		if ([LinphoneManager.instance lpConfigBoolForKey:@"backgroundmode_preference"] || pushNotifEnabled) {
			if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
				// For registration register
				[self refreshRegisters];
			}
		}

		if ([LinphoneManager.instance lpConfigBoolForKey:@"backgroundmode_preference"]) {
			// register keepalive
			if ([[UIApplication sharedApplication]
					setKeepAliveTimeout:600 /*(NSTimeInterval)linphone_proxy_config_get_expires(proxyCfg)*/
								handler:^{
								  LOGW(@"keepalive handler");
								  mLastKeepAliveDate = [NSDate date];
								  if (theLinphoneCore == nil) {
									  LOGW(@"It seems that Linphone BG mode was deactivated, just skipping");
									  return;
								  }
								  
								  if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
									  // For registration register
									  [self refreshRegisters];
								  }
								  linphone_core_iterate(theLinphoneCore);
								}]) {
                                    
				LOGI(@"keepalive handler succesfully registered");
			} else {
				LOGI(@"keepalive handler cannot be registered");
			}
			shouldEnterBgMode = TRUE;
		}
	}

	LinphoneCall *currentCall = linphone_core_get_current_call(theLinphoneCore);
	const bctbx_list_t *callList = linphone_core_get_calls(theLinphoneCore);
	if (!currentCall // no active call
		&& callList  // at least one call in a non active state
		&& bctbx_list_find_custom(callList, (bctbx_compare_func)comp_call_state_paused, NULL)) {
		[self startCallPausedLongRunningTask];
	}
	if (callList) {
		/*if at least one call exist, enter normal bg mode */
		shouldEnterBgMode = TRUE;
	}
	/*stop the video preview*/
	if (theLinphoneCore) {
		linphone_core_enable_video_preview(theLinphoneCore, FALSE);
		linphone_core_iterate(theLinphoneCore);
	}
	linphone_core_stop_dtmf_stream(theLinphoneCore);

	LOGI(@"Entering [%s] bg mode", shouldEnterBgMode ? "normal" : "lite");

	if (!shouldEnterBgMode && floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
		const char *refkey = proxyCfg ? linphone_proxy_config_get_ref_key(proxyCfg) : NULL;
		BOOL pushNotifEnabled = (refkey && strcmp(refkey, "push_notification") == 0);
		if (pushNotifEnabled) {
			LOGI(@"Keeping lc core to handle push");
			/*destroy voip socket if any and reset connectivity mode*/
			connectivity = none;
			linphone_core_set_network_reachable(theLinphoneCore, FALSE);
			return YES;
		}
		return NO;

	} else
		return YES;
}

- (void)becomeActive {
	// enable presence
	if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max || self.connectivity == none) {
		[self refreshRegisters];
	}
	if (pausedCallBgTask) {
		[[UIApplication sharedApplication] endBackgroundTask:pausedCallBgTask];
		pausedCallBgTask = 0;
	}
	if (incallBgTask) {
		[[UIApplication sharedApplication] endBackgroundTask:incallBgTask];
		incallBgTask = 0;
	}

	/*IOS specific*/
	linphone_core_start_dtmf_stream(theLinphoneCore);

	/*start the video preview in case we are in the main view*/
	if (linphone_core_video_display_enabled(theLinphoneCore) && [self lpConfigBoolForKey:@"preview_preference"]) {
		linphone_core_enable_video_preview(theLinphoneCore, TRUE);
	}
	/*check last keepalive handler date*/
	if (mLastKeepAliveDate != Nil) {
		NSDate *current = [NSDate date];
		if ([current timeIntervalSinceDate:mLastKeepAliveDate] > 700) {
			NSString *datestr = [mLastKeepAliveDate description];
			LOGW(@"keepalive handler was called for the last time at %@", datestr);
		}
	}

	[self enableProxyPublish:YES];
}

- (void)beginInterruption {
	LinphoneCall *c = linphone_core_get_current_call(theLinphoneCore);
	LOGI(@"Sound interruption detected!");
	if (c && linphone_call_get_state(c) == LinphoneCallStreamsRunning) {
		linphone_call_pause(c);
	}
}

- (void)endInterruption {
	LOGI(@"Sound interruption ended!");
}

- (void)refreshRegisters {
	if (connectivity == none) {
		// don't trust ios when he says there is no network. Create a new reachability context, the previous one might
		// be mis-functionning.
        LOGI(@"None connectivity");
		[self setupNetworkReachabilityCallback];
	}
    LOGI(@"Network reachability callback setup");
    linphone_core_refresh_registers(theLinphoneCore); // just to make sure REGISTRATION is up to date
}

- (void)renameDefaultSettings {
	// rename .linphonerc to linphonerc to ease debugging: when downloading
	// containers from MacOSX, Finder do not display hidden files leading
	// to useless painful operations to display the .linphonerc file
	NSString *src = [LinphoneManager documentFile:@".linphonerc"];
	NSString *dst = [LinphoneManager documentFile:@"linphonerc"];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *fileError = nil;
	if ([fileManager fileExistsAtPath:src]) {
		if ([fileManager fileExistsAtPath:dst]) {
			[fileManager removeItemAtPath:src error:&fileError];
			LOGW(@"%@ already exists, simply removing %@ %@", dst, src,
				 fileError ? fileError.localizedDescription : @"successfully");
		} else {
			[fileManager moveItemAtPath:src toPath:dst error:&fileError];
			LOGI(@"%@ moving to %@ %@", dst, src, fileError ? fileError.localizedDescription : @"successfully");
		}
	}
}

- (void)copyDefaultSettings {
	NSString *src = [LinphoneManager bundleFile:@"linphonerc"];
	NSString *srcIpad = [LinphoneManager bundleFile:@"linphonerc~ipad"];
	if (IPAD && [[NSFileManager defaultManager] fileExistsAtPath:srcIpad]) {
		src = srcIpad;
	}
	NSString *dst = [LinphoneManager documentFile:@"linphonerc"];
	[LinphoneManager copyFile:src destination:dst override:FALSE];
}

- (void)overrideDefaultSettings {
	NSString *factory = [LinphoneManager bundleFile:@"linphonerc-factory"];
	NSString *factoryIpad = [LinphoneManager bundleFile:@"linphonerc-factory~ipad"];
	if (IPAD && [[NSFileManager defaultManager] fileExistsAtPath:factoryIpad]) {
		factory = factoryIpad;
	}
	NSString *confiFileName = [LinphoneManager documentFile:@"linphonerc"];
	_configDb = lp_config_new_with_factory([confiFileName UTF8String], [factory UTF8String]);
}
#pragma mark - Audio route Functions

- (bool)allowSpeaker {
	if (IPAD)
		return true;

	bool allow = true;
	CFStringRef lNewRoute = CFSTR("Unknown");
	UInt32 lNewRouteSize = sizeof(lNewRoute);
	OSStatus lStatus = AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &lNewRouteSize, &lNewRoute);
	if (!lStatus && lNewRouteSize > 0) {
		NSString *route = (__bridge NSString *)lNewRoute;
		allow = ![self containsWithSelfStr:route Substring:@"Heads"] && ![self containsWithSelfStr:route Substring:@"Lineout"];
        
		CFRelease(lNewRoute);
	}
	return allow;
}

// 音频通道改变, 就是听筒扬声器
- (void)audioRouteChangeListenerCallback:(NSNotification *)notif {
	if (IPAD)
		return;

	// there is at least one bug when you disconnect an audio bluetooth headset
	// since we only get notification of route having changed, we cannot tell if that is due to:
	// -bluetooth headset disconnected or
	// -user wanted to use earpiece
	// the only thing we can assume is that when we lost a device, it must be a bluetooth one (strong hypothesis though)
	if ([[notif.userInfo valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue] ==
		AVAudioSessionRouteChangeReasonOldDeviceUnavailable) {
		_bluetoothAvailable = NO;
	}

	CFStringRef newRoute = CFSTR("Unknown");
	UInt32 newRouteSize = sizeof(newRoute);

	OSStatus status = AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &newRouteSize, &newRoute);
	if (!status && newRouteSize > 0) {
		NSString *route = (__bridge NSString *)newRoute;
		LOGI(@"Current audio route is [%s]", [route UTF8String]);

		_speakerEnabled = [route isEqualToString:@"Speaker"] || [route isEqualToString:@"SpeakerAndMicrophone"];
		if ([route isEqualToString:@"HeadsetBT"] && !_speakerEnabled) {
			_bluetoothAvailable = TRUE;
			_bluetoothEnabled = TRUE;
		} else {
			_bluetoothEnabled = FALSE;
		}
		NSDictionary *dict = [NSDictionary
			dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:_bluetoothAvailable], @"available", nil];
		[NSNotificationCenter.defaultCenter postNotificationName:kLinphoneBluetoothAvailabilityUpdate
														  object:self
														userInfo:dict];
		CFRelease(newRoute);
	}
}

// 听筒扬声器
- (void)setSpeakerEnabled:(BOOL)enable {
	OSStatus ret = 0;
	_speakerEnabled = enable;
	UInt32 override = kAudioSessionUnspecifiedError;
    
	if (!enable && _bluetoothAvailable) {
		UInt32 bluetoothInputOverride = _bluetoothEnabled;
		ret = AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryEnableBluetoothInput,
									  sizeof(bluetoothInputOverride), &bluetoothInputOverride);
		// if setting bluetooth failed, it must be because the device is not available
		// anymore (disconnected), so deactivate bluetooth.
		if (ret != kAudioSessionNoError) {
			_bluetoothAvailable = _bluetoothEnabled = FALSE;
		}
	}
    
	if (override != kAudioSessionNoError) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        NSError *error = nil;
        // YES扬声器, NO听筒
        AVAudioSessionPortOverride override = 0;
        
		if (enable && [self allowSpeaker]) {
			_bluetoothEnabled = FALSE;
            override = AVAudioSessionPortOverrideSpeaker;
            
		} else {
            override = AVAudioSessionPortOverrideNone;
		}
        
        [session overrideOutputAudioPort:override error:&error];
        if (error) {
            LOGE(@"avaudio session error %@", error);
        }
	}

	if (ret != kAudioSessionNoError) {
		LOGE(@"Failed to change audio route: err %d", ret);
	}
    
}

- (void)setBluetoothEnabled:(BOOL)enable {
	if (_bluetoothAvailable) {
		// The change of route will be done in setSpeakerEnabled
		_bluetoothEnabled = enable;
		[self setSpeakerEnabled:!_bluetoothEnabled && _speakerEnabled];
	}
}

#pragma mark - Call Functions

// 接听
- (void)acceptCall:(LinphoneCall *)call evenWithVideo:(BOOL)video {
	LinphoneCallParams *lcallParams = linphone_core_create_call_params(theLinphoneCore, call);
	if (!lcallParams) {
		LOGW(@"Could not create call parameters for %p, call has probably already ended.", call);
		return;
	}
    
	if ([self lpConfigBoolForKey:@"edge_opt_preference"]) {
		bool low_bandwidth = self.network == network_2g;
		if (low_bandwidth) {
			LOGI(@"Low bandwidth mode");
		}
		linphone_call_params_enable_low_bandwidth(lcallParams, low_bandwidth);
	}
    // 视频收发
    linphone_call_params_set_video_direction(lcallParams, self.dir);
    // 是否使用视频流
	linphone_call_params_enable_video(lcallParams, video);
    // 接听
	linphone_call_accept_with_params(call, lcallParams);
}


// 呼叫
- (BOOL)call:(const LinphoneAddress *)iaddr {
	// 确认网络
	if (!linphone_core_is_network_reachable(theLinphoneCore)) {
        LOGI(@"网络不可用");
		return FALSE;
	}

	// 当前是否在进行系统(GSM)通话
	CTCallCenter *callCenter = [[CTCallCenter alloc] init];
	if ([callCenter currentCalls] != nil) {
        LOGI(@"正在进行GSM通话");
		return FALSE;
	}

	// 地址是否为空
	if (!iaddr) {
        LOGI(@"地址为空");
		return FALSE;
	}
    
	LinphoneAddress *addr = linphone_address_clone(iaddr);
    
	// 创建呼叫参数
	LinphoneCallParams *lcallParams = linphone_core_create_call_params(theLinphoneCore, NULL);
    
    // 网络2G
	if ([self lpConfigBoolForKey:@"edge_opt_preference"] && (self.network == network_2g)) {
		LOGI(@"Enabling low bandwidth mode");
		linphone_call_params_enable_low_bandwidth(lcallParams, YES);
	}
    
    // 设置domain
	if ([LinphoneManager.instance lpConfigBoolForKey:@"override_domain_with_default_one"]) {
		linphone_address_set_domain(addr, [[LinphoneManager.instance lpConfigStringForKey:@"domain" inSection:@"assistant"] UTF8String]);
	}
    
    // 设置视频的收发
     linphone_call_params_set_video_direction(lcallParams, self.dir);
    // 允许视频流
    linphone_call_params_enable_video(lcallParams, self.enableVideo);
    
    // 通话转移？如果外部没改变，这个通话转移是一直不会进去的
	if (LinphoneManager.instance.nextCallIsTransfer) {
		char *caddr = linphone_address_as_string(addr);
		linphone_call_transfer(linphone_core_get_current_call(theLinphoneCore), caddr);
		LinphoneManager.instance.nextCallIsTransfer = NO;
		ms_free(caddr);
        
	} else {
        
        // 呼叫
		LinphoneCall *call = linphone_core_invite_address_with_params(theLinphoneCore, addr, lcallParams);
        
		if (call) {
            /** 总体来说，就是需要创建LinphoneCallAppData, 而且要实现 - (void)onCall:StateChanged:withMessage:, 不然会crash */
			// The LinphoneCallAppData object should be set on call creation with callback
			// - (void)onCall:StateChanged:withMessage:. If not, we are in big trouble and expect it to crash
			// We are NOT responsible for creating the AppData.
			LinphoneCallAppData *data = (__bridge LinphoneCallAppData *)linphone_call_get_user_data(call);
			if (data == nil) {
				LOGE(@"New call instanciated but app data was not set. Expect it to crash.");
				/* will be used later to notify user if video was not activated because of the linphone core*/
			} else {
				data->videoRequested = linphone_call_params_video_enabled(lcallParams);
			}
		}
        
	}
    
    // 销毁地址和呼叫参数
	linphone_address_unref(addr);
	linphone_call_params_unref(lcallParams);

	return TRUE;
}

#pragma mark - Misc Functions

+ (NSString *)bundleFile:(NSString *)file {
	return [[NSBundle mainBundle] pathForResource:[file stringByDeletingPathExtension] ofType:[file pathExtension]];
}

+ (NSString *)documentFile:(NSString *)file {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsPath = [paths objectAtIndex:0];
	return [documentsPath stringByAppendingPathComponent:file];
}

+ (NSString *)cacheDirectory {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	NSString *cachePath = [paths objectAtIndex:0];
	BOOL isDir = NO;
	NSError *error;
	// cache directory must be created if not existing
	if (![[NSFileManager defaultManager] fileExistsAtPath:cachePath isDirectory:&isDir] && isDir == NO) {
		[[NSFileManager defaultManager] createDirectoryAtPath:cachePath
								  withIntermediateDirectories:NO
												   attributes:nil
														error:&error];
	}
	return cachePath;
}

+ (int)unreadMessageCount {
	int count = 0;
	const MSList *rooms = linphone_core_get_chat_rooms(LC);
	const MSList *item = rooms;
	while (item) {
		LinphoneChatRoom *room = (LinphoneChatRoom *)item->data;
		if (room) {
			count += linphone_chat_room_get_unread_messages_count(room);
		}
		item = item->next;
	}

	return count;
}

+ (BOOL)copyFile:(NSString *)src destination:(NSString *)dst override:(BOOL)override {
	NSFileManager *fileManager = NSFileManager.defaultManager;
	NSError *error = nil;
	if ([fileManager fileExistsAtPath:src] == NO) {
		LOGE(@"Can't find \"%@\": %@", src, [error localizedDescription]);
		return FALSE;
	}
	if ([fileManager fileExistsAtPath:dst] == YES) {
		if (override) {
			[fileManager removeItemAtPath:dst error:&error];
			if (error != nil) {
				LOGE(@"Can't remove \"%@\": %@", dst, [error localizedDescription]);
				return FALSE;
			}
		} else {
			LOGW(@"\"%@\" already exists", dst);
			return FALSE;
		}
	}
	[fileManager copyItemAtPath:src toPath:dst error:&error];
	if (error != nil) {
		LOGE(@"Can't copy \"%@\" to \"%@\": %@", src, dst, [error localizedDescription]);
		return FALSE;
	}
	return TRUE;
}

- (void)configureVbrCodecs {
	LinphonePayloadType *pt;
	int bitrate = lp_config_get_int(
		_configDb, "audio", "codec_bitrate_limit",
		kLinphoneAudioVbrCodecDefaultBitrate); /*default value is in linphonerc or linphonerc-factory*/
	const MSList *audio_codecs = linphone_core_get_audio_payload_types(theLinphoneCore);
	const MSList *codec = audio_codecs;
	while (codec) {
		pt = codec->data;
		if (linphone_payload_type_is_vbr(pt)) {
			linphone_payload_type_set_normal_bitrate(pt, bitrate);
		}
		codec = codec->next;
	}
}

+ (id)getMessageAppDataForKey:(NSString *)key inMessage:(LinphoneChatMessage *)msg {

	if (msg == nil)
		return nil;

	id value = nil;
	const char *appData = linphone_chat_message_get_appdata(msg);
	if (appData) {
		NSDictionary *appDataDict =
			[NSJSONSerialization JSONObjectWithData:[NSData dataWithBytes:appData length:strlen(appData)]
											options:0
											  error:nil];
		value = [appDataDict objectForKey:key];
	}
	return value;
}

+ (void)setValueInMessageAppData:(id)value forKey:(NSString *)key inMessage:(LinphoneChatMessage *)msg {

	NSMutableDictionary *appDataDict = [NSMutableDictionary dictionary];
	const char *appData = linphone_chat_message_get_appdata(msg);
	if (appData) {
		appDataDict = [NSJSONSerialization JSONObjectWithData:[NSData dataWithBytes:appData length:strlen(appData)]
													  options:NSJSONReadingMutableContainers
														error:nil];
	}

	[appDataDict setValue:value forKey:key];

	NSData *data = [NSJSONSerialization dataWithJSONObject:appDataDict options:0 error:nil];
	NSString *appdataJSON = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	linphone_chat_message_set_appdata(msg, [appdataJSON UTF8String]);
}

#pragma mark - LPConfig Functions

- (void)lpConfigSetString:(NSString *)value forKey:(NSString *)key {
	[self lpConfigSetString:value forKey:key inSection:LINPHONERC_APPLICATION_KEY];
}
- (void)lpConfigSetString:(NSString *)value forKey:(NSString *)key inSection:(NSString *)section {
	if (!key)
		return;
	lp_config_set_string(_configDb, [section UTF8String], [key UTF8String], value ? [value UTF8String] : NULL);
}
- (NSString *)lpConfigStringForKey:(NSString *)key {
	return [self lpConfigStringForKey:key withDefault:nil];
}
- (NSString *)lpConfigStringForKey:(NSString *)key withDefault:(NSString *)defaultValue {
	return [self lpConfigStringForKey:key inSection:LINPHONERC_APPLICATION_KEY withDefault:defaultValue];
}
- (NSString *)lpConfigStringForKey:(NSString *)key inSection:(NSString *)section {
	return [self lpConfigStringForKey:key inSection:section withDefault:nil];
}
- (NSString *)lpConfigStringForKey:(NSString *)key inSection:(NSString *)section withDefault:(NSString *)defaultValue {
	if (!key)
		return defaultValue;
	const char *value = lp_config_get_string(_configDb, [section UTF8String], [key UTF8String], NULL);
	return value ? [NSString stringWithUTF8String:value] : defaultValue;
}

- (void)lpConfigSetInt:(int)value forKey:(NSString *)key {
	[self lpConfigSetInt:value forKey:key inSection:LINPHONERC_APPLICATION_KEY];
}
- (void)lpConfigSetInt:(int)value forKey:(NSString *)key inSection:(NSString *)section {
	if (!key)
		return;
	lp_config_set_int(_configDb, [section UTF8String], [key UTF8String], (int)value);
}
- (int)lpConfigIntForKey:(NSString *)key {
	return [self lpConfigIntForKey:key withDefault:-1];
}
- (int)lpConfigIntForKey:(NSString *)key withDefault:(int)defaultValue {
	return [self lpConfigIntForKey:key inSection:LINPHONERC_APPLICATION_KEY withDefault:defaultValue];
}
- (int)lpConfigIntForKey:(NSString *)key inSection:(NSString *)section {
	return [self lpConfigIntForKey:key inSection:section withDefault:-1];
}
- (int)lpConfigIntForKey:(NSString *)key inSection:(NSString *)section withDefault:(int)defaultValue {
	if (!key)
		return defaultValue;
	return lp_config_get_int(_configDb, [section UTF8String], [key UTF8String], (int)defaultValue);
}

- (void)lpConfigSetBool:(BOOL)value forKey:(NSString *)key {
	[self lpConfigSetBool:value forKey:key inSection:LINPHONERC_APPLICATION_KEY];
}
- (void)lpConfigSetBool:(BOOL)value forKey:(NSString *)key inSection:(NSString *)section {
	[self lpConfigSetInt:(int)(value == TRUE) forKey:key inSection:section];
}
- (BOOL)lpConfigBoolForKey:(NSString *)key {
	return [self lpConfigBoolForKey:key withDefault:FALSE];
}
- (BOOL)lpConfigBoolForKey:(NSString *)key withDefault:(BOOL)defaultValue {
	return [self lpConfigBoolForKey:key inSection:LINPHONERC_APPLICATION_KEY withDefault:defaultValue];
}
- (BOOL)lpConfigBoolForKey:(NSString *)key inSection:(NSString *)section {
	return [self lpConfigBoolForKey:key inSection:section withDefault:FALSE];
}
- (BOOL)lpConfigBoolForKey:(NSString *)key inSection:(NSString *)section withDefault:(BOOL)defaultValue {
	if (!key)
		return defaultValue;
	int val = [self lpConfigIntForKey:key inSection:section withDefault:-1];
	return (val != -1) ? (val == 1) : defaultValue;
}

#pragma mark - GSM management

- (void)removeCTCallCenterCb {
	if (mCallCenter != nil) {
		LOGI(@"Removing CT call center listener [%p]", mCallCenter);
		mCallCenter.callEventHandler = NULL;
	}
	mCallCenter = nil;
}

// 监听GSM电话
- (void)setupGSMInteraction {
	[self removeCTCallCenterCb];
	mCallCenter = [[CTCallCenter alloc] init];
	LOGI(@"Adding CT call center listener [%p]", mCallCenter);
	__block __weak LinphoneManager *weakSelf = self;
	__block __weak CTCallCenter *weakCCenter = mCallCenter;
	mCallCenter.callEventHandler = ^(CTCall *call) {
	  // post on main thread
	  [weakSelf performSelectorOnMainThread:@selector(handleGSMCallInteration:)
								 withObject:weakCCenter
							  waitUntilDone:YES];
	};
}

// 当GSM呼叫过来，暂停sip电话
- (void)handleGSMCallInteration:(id)cCenter {
	CTCallCenter *ct = (CTCallCenter *)cCenter;
	/* pause current call, if any */
	LinphoneCall *call = linphone_core_get_current_call(theLinphoneCore);
	if ([ct currentCalls] != nil) {
		if (call) {
			LOGI(@"Pausing SIP call because GSM call");
            // 暂停通话
			//linphone_call_pause(call);
            // 挂断所有通话
            linphone_core_terminate_all_calls(LC);
			[self startCallPausedLongRunningTask];
            
            // 是否在会议呼叫?多人通话？
		} else if (linphone_core_is_in_conference(theLinphoneCore)) {
			LOGI(@"Leaving conference call because GSM call");
            // 离开会议通话
			linphone_core_leave_conference(theLinphoneCore);
			[self startCallPausedLongRunningTask];
		}
	} // else nop, keep call in paused state
}

- (NSString *)contactFilter {
	NSString *filter = @"*";
	if ([self lpConfigBoolForKey:@"contact_filter_on_default_domain"]) {
		LinphoneProxyConfig *proxy_cfg = linphone_core_get_default_proxy_config(theLinphoneCore);
		if (proxy_cfg && linphone_proxy_config_get_addr(proxy_cfg)) {
			return [NSString stringWithCString:linphone_proxy_config_get_domain(proxy_cfg)
									  encoding:[NSString defaultCStringEncoding]];
		}
	}
	return filter;
}

#pragma mark -

- (void)removeAllAccounts {
    // 清除代理
    linphone_core_clear_proxy_config(LC);
    // 清除认证信息
    linphone_core_clear_all_auth_info(LC);
}

+ (BOOL)isMyself:(const LinphoneAddress *)addr {
	if (!addr)
		return NO;

	const MSList *it = linphone_core_get_proxy_config_list(LC);
	while (it) {
		if (linphone_address_weak_equal(addr, linphone_proxy_config_get_identity_address(it->data))) {
			return YES;
		}
		it = it->next;
	}
	return NO;
}

// ugly hack to export symbol from liblinphone so that they are available for the linphoneTests target
// linphoneTests target do not link with liblinphone but instead dynamically link with ourself which is
// statically linked with liblinphone, so we must have exported required symbols from the library to
// have them available in linphoneTests
// DO NOT INVOKE THIS METHOD
- (void)exportSymbolsForUITests {
	linphone_address_set_header(NULL, NULL, NULL);
}

#pragma mark -- 获取设备类型
+ (NSString *)deviceModelIdentifier {
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *machine = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([machine isEqual:@"iPad1,1"])
        return @"iPad";
    else if ([machine isEqual:@"iPad2,1"])
        return @"iPad 2";
    else if ([machine isEqual:@"iPad2,2"])
        return @"iPad 2";
    else if ([machine isEqual:@"iPad2,3"])
        return @"iPad 2";
    else if ([machine isEqual:@"iPad2,4"])
        return @"iPad 2";
    else if ([machine isEqual:@"iPad3,1"])
        return @"iPad 3";
    else if ([machine isEqual:@"iPad3,2"])
        return @"iPad 3";
    else if ([machine isEqual:@"iPad3,3"])
        return @"iPad 3";
    else if ([machine isEqual:@"iPad3,4"])
        return @"iPad 4";
    else if ([machine isEqual:@"iPad3,5"])
        return @"iPad 4";
    else if ([machine isEqual:@"iPad3,6"])
        return @"iPad 4";
    else if ([machine isEqual:@"iPad4,1"])
        return @"iPad Air";
    else if ([machine isEqual:@"iPad4,2"])
        return @"iPad Air";
    else if ([machine isEqual:@"iPad4,3"])
        return @"iPad Air";
    else if ([machine isEqual:@"iPad5,3"])
        return @"iPad Air 2";
    else if ([machine isEqual:@"iPad5,4"])
        return @"iPad Air 2";
    else if ([machine isEqual:@"iPad6,7"])
        return @"iPad Pro 12.9";
    else if ([machine isEqual:@"iPad6,8"])
        return @"iPad Pro 12.9";
    else if ([machine isEqual:@"iPad6,3"])
        return @"iPad Pro 9.7";
    else if ([machine isEqual:@"iPad6,4"])
        return @"iPad Pro 9.7";
    else if ([machine isEqual:@"iPad2,5"])
        return @"iPad mini";
    else if ([machine isEqual:@"iPad2,6"])
        return @"iPad mini";
    else if ([machine isEqual:@"iPad2,7"])
        return @"iPad mini";
    else if ([machine isEqual:@"iPad4,4"])
        return @"iPad mini 2";
    else if ([machine isEqual:@"iPad4,5"])
        return @"iPad mini 2";
    else if ([machine isEqual:@"iPad4,6"])
        return @"iPad mini 2";
    else if ([machine isEqual:@"iPad4,7"])
        return @"iPad mini 3";
    else if ([machine isEqual:@"iPad4,8"])
        return @"iPad mini 3";
    else if ([machine isEqual:@"iPad4,9"])
        return @"iPad mini 3";
    else if ([machine isEqual:@"iPad5,1"])
        return @"iPad mini 4";
    else if ([machine isEqual:@"iPad5,2"])
        return @"iPad mini 4";
    
    else if ([machine isEqual:@"iPhone1,1"])
        return @"iPhone";
    else if ([machine isEqual:@"iPhone1,2"])
        return @"iPhone 3G";
    else if ([machine isEqual:@"iPhone2,1"])
        return @"iPhone 3GS";
    else if ([machine isEqual:@"iPhone3,1"])
        return @"iPhone 4";
    else if ([machine isEqual:@"iPhone3,2"])
        return @"iPhone 4";
    else if ([machine isEqual:@"iPhone3,3"])
        return @"iPhone 4";
    else if ([machine isEqual:@"iPhone4,1"])
        return @"iPhone 4S";
    else if ([machine isEqual:@"iPhone5,1"])
        return @"iPhone5,2	iPhone 5";
    else if ([machine isEqual:@"iPhone5,3"])
        return @"iPhone5,4	iPhone 5c";
    else if ([machine isEqual:@"iPhone6,1"])
        return @"iPhone6,2	iPhone 5s";
    else if ([machine isEqual:@"iPhone7,2"])
        return @"iPhone 6";
    else if ([machine isEqual:@"iPhone7,1"])
        return @"iPhone 6 Plus";
    else if ([machine isEqual:@"iPhone8,1"])
        return @"iPhone 6s";
    else if ([machine isEqual:@"iPhone8,2"])
        return @"iPhone 6s Plus";
    else if ([machine isEqual:@"iPhone8,4"])
        return @"iPhone SE";
    
    else if ([machine isEqual:@"iPod1,1"])
        return @"iPod touch";
    else if ([machine isEqual:@"iPod2,1"])
        return @"iPod touch 2G";
    else if ([machine isEqual:@"iPod3,1"])
        return @"iPod touch 3G";
    else if ([machine isEqual:@"iPod4,1"])
        return @"iPod touch 4G";
    else if ([machine isEqual:@"iPod5,1"])
        return @"iPod touch 5G";
    else if ([machine isEqual:@"iPod7,1"])
        return @"iPod touch 6G";
    
    else if ([machine isEqual:@"x86_64"])
        return @"simulator 64bits";
    
    // none matched: cf https://www.theiphonewiki.com/wiki/Models for the whole list
    LOGW(@"%s: Oops, unknown machine %@... consider completing me!", __FUNCTION__, machine);
    return machine;
}

- (BOOL)containsWithSelfStr:(NSString *)selfStr Substring:(NSString *)str {
    if (UIDevice.currentDevice.systemVersion.doubleValue >= 8.0) {
#pragma deploymate push "ignored-api-availability"
        return [selfStr containsString:str];
#pragma deploymate pop
    }
    return ([selfStr rangeOfString:str].location != NSNotFound);
}



@end




