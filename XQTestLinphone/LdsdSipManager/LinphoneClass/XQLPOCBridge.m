//
//  XQLPOCBridge.m
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/10.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import "XQLPOCBridge.h"

@implementation XQLPOCBridge

+ (NSString *)getMessageWithChatMessage:(const LinphoneChatMessage *)chatMessage {
    NSString *message = @"";
    if (!chatMessage) {
        return message;
    }
    
    const char *text = linphone_chat_message_get_text(chatMessage);
    if (text) {
        message = [NSString stringWithCString:text encoding:NSUTF8StringEncoding];
    }
    return message;
}

+ (NSString *)getDisplayNameWithAddress:(const LinphoneAddress *)address {
    NSString *displayeNameStr = @"";
    if (!address) {
        return displayeNameStr;
    }
    
    const char *displayeName = linphone_address_get_display_name(address);
    if (displayeName) {
        displayeNameStr = [NSString stringWithCString:displayeName encoding:NSUTF8StringEncoding];
    }
    
    return displayeNameStr;
}

+ (NSString *)getUserNameWithAddress:(const LinphoneAddress *)address {
    NSString *usernameStr = @"";
    if (!address) {
        return usernameStr;
    }
    
    const char *username = linphone_address_get_username(address);
    if (username) {
        usernameStr = [NSString stringWithCString:username encoding:NSUTF8StringEncoding];
    }
    
    return usernameStr;
}

+ (NSString *)getDomainWithAddress:(const LinphoneAddress *)address {
    NSString *domainStr = @"";
    if (!address) {
        return domainStr;
    }
    
    const char *domain = linphone_address_get_domain(address);
    if (domain) {
        domainStr = [NSString stringWithCString:domain encoding:NSUTF8StringEncoding];
    }
    
    return domainStr;
}

+ (NSString *)getMessageIDWithChatMessage:(const LinphoneChatMessage *)chatMessage {
    NSString *messageIDStr = @"";
    if (!chatMessage) {
        return messageIDStr;
    }
    
    // 获取该消息的id
    const char *messageID = linphone_chat_message_get_message_id(chatMessage);
    if (messageID) {
        messageIDStr = [NSString stringWithCString:messageID encoding:NSUTF8StringEncoding];
    }
    
    return messageIDStr;
}



@end










