//
//  LdsdSipManager.m
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/5.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import "LdsdSipManager.h"
#import "LinphoneManager.h"

/** 通话状态
 LinphoneCallIdle,				// 通话初始化
 LinphoneCallIncomingReceived,   // 收到来电
 LinphoneCallOutgoingInit,       // 呼出电话初始化
 LinphoneCallOutgoingProgress,   // 呼出电话拨号中
 LinphoneCallOutgoingRinging,    // 呼出电话正在响铃
 LinphoneCallOutgoingEarlyMedia, // An outgoing call is proposed early media
 LinphoneCallConnected,          // 通话连接成功
 LinphoneCallStreamsRunning,     // 媒体流已建立
 LinphoneCallPausing,            // 通话暂停中
 LinphoneCallPaused,             // 通话暂停成功
 LinphoneCallResuming,           // 通话被恢复
 LinphoneCallRefered,            // 通话转移
 LinphoneCallError,              // 通话错误
 LinphoneCallEnd,                // 通话正常结束
 LinphoneCallPausedByRemote,     // 通话被对方暂停
 LinphoneCallUpdatedByRemote,    // 对方请求更新通话参数
 LinphoneCallIncomingEarlyMedia, // We are proposing early media to an incoming call
 LinphoneCallUpdating,           // A call update has been initiated by us
 LinphoneCallReleased,           // 通话被释放
 LinphoneCallEarlyUpdatedByRemote, // 通话未应答
 LinphoneCallEarlyUpdating       // 通话未应答我方
 */

@interface LdsdSipManager ()

/** 判断是否是通过manager来创建对象 */
@property (nonatomic, assign) BOOL isManager;

/** 正在通话中, 有其他来电, 记录拒绝数组, 然后挂断和release不回调出去 */
@property (nonatomic, strong) NSMutableArray *declineSipArr;
/** 5秒查询一次是否有通话, 如没有通话, 则删除拒绝数组 */
@property (nonatomic, strong) NSTimer *timer;

@end

static LdsdSipManager *_manager = nil;

#define LM [LdsdSipManager manager]
#define NC [NSNotificationCenter defaultCenter]

@implementation LdsdSipManager

+ (instancetype)manager {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!_manager) {
            _manager = [LdsdSipManager new];
            _manager.isManager = YES;
        }
    });
    
    return _manager;
}

- (BOOL)lcIsExist {
    if (!LC) {
        LOG(@"not init");
        return NO;
    }
    
    // 设置呼叫超时
//    linphone_core_set_in_call_timeout(<#LinphoneCore *lc#>, <#int seconds#>)
    // 设置来电超时
//    linphone_core_set_inc_timeout(<#LinphoneCore *lc#>, <#int seconds#>)
    // 设置RTP超时
//    linphone_core_set_nortp_timeout(<#LinphoneCore *lc#>, <#int seconds#>)
    // 设置延时超时
//    linphone_core_set_delayed_timeout(<#LinphoneCore *lc#>, <#int seconds#>)
    // 设置sip传输超时
//    linphone_core_set_sip_transport_timeout(<#LinphoneCore *lc#>, <#int timeout_ms#>)
    
//    linphone_core_get_video_codecs(<#const LinphoneCore *lc#>)
    
    return YES;
}

#pragma mark -- 初始化
- (void)initSipServer {
    // 是否通过manager初始化
    if (!self.isManager) {
        LOG(@"not manager");
        return;
    }
    
//    [NC addObserver:self selector:@selector(noticeGlobal:) name:kLinphoneGlobalStateUpdate object:nil];
    // kLinphoneCoreInitComplete 这个是自定义的通知, 当core初始化完成时发送
    [NC addObserver:self selector:@selector(noticeRegistration:) name:kLinphoneRegistrationUpdate object:nil];
    [NC addObserver:self selector:@selector(noticeCallUpdate:) name:kLinphoneCallUpdate object:nil];
    [NC addObserver:self selector:@selector(noticeCallChange:) name:kLinphoneCallChange object:nil];
    [NC addObserver:self selector:@selector(noticeMessageReceived:) name:kLinphoneMessageReceived object:nil];
    
    [[LinphoneManager instance] startLinphoneCore];
    
    // 设置视频开启和视频传输方向
    [self setVideoEnable:YES];
    [self setVideoWithDir:LdsdSipMediaDirectionRecvOnly];
    
    [self createTimer];
}

- (void)enableLogs:(BOOL)enable {
    LinphoneManager.instance.logLevel = enable ? ORTP_DEBUG : ORTP_LOGLEV_END;
    enable ? [Log enableLogs:ORTP_DEBUG] : [Log enableLogs:ORTP_LOGLEV_END];
}

#pragma mark -- 登录
/** 登陆
 @param userName  用户名
 @param password  密码
 @param displayName  昵称
 @param domain    域名或IP
 @param port      端口
 @param transport 连接方式
 */
- (BOOL)ldsdLogin:(NSString *)userName password:(NSString *)password displayName:(NSString *)displayName domain:(NSString *)domain port:(NSString *)port withTransport:(NSString *)transport {
    if (!userName || [userName isEqualToString:@""] || !password || [password isEqualToString:@""] || !displayName || [displayName isEqualToString:@""] || !domain || [domain isEqualToString:@""] || !port || [port isEqualToString:@""] || !transport || [transport isEqualToString:@""]) {
        LOG(@"params error");
        return NO;
    }
    
    // core是否存在
    if (![self lcIsExist]) return NO;
    
    // 创建代理设置
    LinphoneProxyConfig* proxyCfg = linphone_core_create_proxy_config(LC);
    NSString* server_address = domain;
//    linphone_account_creator_create_proxy_config(<#const LinphoneAccountCreator *creator#>)
    
    // 初始化电话号码
    linphone_proxy_config_normalize_phone_number(proxyCfg, userName.UTF8String);
    
    // 地址字符串: sip:username@domain
    const char *identity = [NSString stringWithFormat:@"sip:%@@%@", userName, domain].UTF8String;
    
    // 根据地址字符串获取address
    LinphoneAddress* linphoneAddress = linphone_address_new(identity);
    if (!linphoneAddress) {
        LOG(@"address no exist");
        return NO;
    }
    
    BOOL isSip = linphone_address_is_sip(linphoneAddress);
    if (!isSip) {
        LOG(@"address not sip");
        return NO;
    }
    
    linphone_address_set_username(linphoneAddress, userName.UTF8String);
    
    // 设置昵称
    if (displayName && displayName.length != 0) {
        linphone_address_set_display_name(linphoneAddress, displayName.UTF8String);
    }
    
    // 设置服务
    if(domain && [domain length] != 0) {
        if( transport != nil ){
            // 服务地址:  domain:port;transport=udp
            server_address = [NSString stringWithFormat:@"%@:%@;transport=%@", server_address, port, [transport lowercaseString]];
        }
        // 设置服务地址和地址的域名
        // when the domain is specified (for external login), take it as the server address
        linphone_proxy_config_set_server_addr(proxyCfg, [server_address UTF8String]);
        linphone_address_set_domain(linphoneAddress, [domain UTF8String]);
    }
    
    // 添加了昵称后的identity(这一步好像可以省略)
    char* ch_identity = linphone_address_as_string(linphoneAddress);
    if (!ch_identity) {
        LOG(@"as string error");
        return NO;
    }
    // 释放标识符
    ms_free(ch_identity);
    
    /** 创建认证信息
     *  账号
     *  userid
     *  密码
     *  ha1密码加密
     *  认证域
     *  SIP域
     */
    LinphoneAuthInfo* info = linphone_auth_info_new([userName UTF8String],
                                                    NULL,
                                                    [password UTF8String],
                                                    NULL,
                                                    linphone_proxy_config_get_realm(proxyCfg),
                                                    linphone_proxy_config_get_domain(proxyCfg));
    
    if (!info) {
        LOG(@"info not exist");
        return NO;
    }
    
    // 设置一个SIP路线  外呼必经之路 sip:semtec-test.weedoor.com:9647
    linphone_proxy_config_set_route(proxyCfg, [NSString stringWithFormat:@"sip:%@:%@", domain, port].UTF8String);
    // 设置代理的地址
    linphone_proxy_config_set_identity_address(proxyCfg, linphoneAddress);
    // 有效期
    linphone_proxy_config_set_expires(proxyCfg, 2000);
    // 可注册
    linphone_proxy_config_enable_register(proxyCfg, true);
    // 添加认证信息
    linphone_core_add_auth_info(LC, info);
    // 添加代理设置
    int i = linphone_core_add_proxy_config(LC, proxyCfg);
    if (i == -1) {
        LOG(@"add proxy cfg fail");
        return NO;
    }
    
    // 设置默认代理设置
    linphone_core_set_default_proxy_config(LC, proxyCfg);
    
    /*
    //设置传输方式,默认是UDP
    char *proxy = ms_strdup(identity);
    LinphoneAddress *proxy_addr = linphone_core_interpret_url(LC, proxy);
    if (proxy_addr) {
        LinphoneTransportType type = LinphoneTransportUdp;
        if ([transport isEqualToString:@"tcp"])
            type = LinphoneTransportTcp;
        else if ([transport isEqualToString:@"tls"])
            type = LinphoneTransportTls;
        
        linphone_address_set_transport(proxy_addr, type);
        ms_free(proxy);
        proxy = linphone_address_as_string_uri_only(proxy_addr);
    }
     */
    
    // 销毁address
    linphone_address_unref(linphoneAddress);
    
    return YES;
}

#pragma mark -- 登出某一个账号
- (BOOL)ldsdLogout:(NSString *)userName domain:(NSString *)domain {
    return [self ldsdLogout:userName domain:domain isAPI:YES];
}

- (BOOL)ldsdLogout:(NSString *)userName domain:(NSString *)domain isAPI:(BOOL)isAPI {
    if (!userName || [userName isEqualToString:@""] || !domain || [domain isEqualToString:@""]) {
        LOG(@"logout fail, param error");
        return NO;
    }
    
    if (![self lcIsExist]) return NO;
    
    // 单个账号登陆登出
    
    LinphoneProxyConfig *pCfg = nil;
    // 基本linphone的列表都是这样, 比如消息列表也一样可以这样获取消息
    const MSList *proxies = linphone_core_get_proxy_config_list(LC);
    while (proxies) {
        LinphoneProxyConfig *cfg = (LinphoneProxyConfig *)proxies->data;
        const char *cfgDomain = linphone_proxy_config_get_domain(cfg);
        const LinphoneAddress *addr = linphone_proxy_config_get_identity_address(cfg);
        const char *cfgUserName = linphone_address_get_username(addr);
        
        // strcmp(a, b) 比较两个字符串 a==b返回0, a<b返回负数, a>b返回正数
        if (addr && (cfgUserName && strcmp(cfgUserName, userName.UTF8String) == 0) &&
            (cfgDomain && strcmp(cfgDomain, domain.UTF8String) == 0)) {
            pCfg = cfg;
            break;
        }
        
        proxies = proxies->next;
    }
    
    if (!pCfg) {
        LOG(@"cfg not exist");
        return NO;
    }
    
    // 必须先删除proxy, 再删除authInfo
    linphone_core_remove_proxy_config(LC, pCfg);
    
    const LinphoneAuthInfo *info = linphone_core_find_auth_info(LC, nil, userName.UTF8String, domain.UTF8String);
    if (!info) {
        LOG(@"info not exist");
        return NO;
    }
    linphone_core_remove_auth_info(LC, info);
    
    return YES;
}

#pragma mark -- 登出所有
- (BOOL)ldsdLogoutAll {
    if (![self lcIsExist]) return NO;
    
    // 清除 config and auth_info
    [[LinphoneManager instance] removeAllAccounts];
    
    /*
     [[LinphoneManager instance] lpConfigSetBool:FALSE forKey:@"pushnotification_preference"];
     [[LinphoneManager instance] lpConfigSetString:@"" forKey:@"sharing_server_preference"];
     [[LinphoneManager instance] lpConfigSetBool:FALSE forKey:@"ice_preference"];
     [[LinphoneManager instance] lpConfigSetString:@"" forKey:@"stun_preference"];
     */
    
    return YES;
}

#pragma mark -- 发送消息
- (NSString *)ldsdSendMessage:(NSString *)to message:(NSString *)message {
    if (!to || [to isEqualToString:@""] || !message || [message isEqualToString:@""]) {
        LOG(@"param error");
        return nil;
    }
    
    if (![self lcIsExist]) return nil;
    
    LinphoneProxyConfig *pCfg = linphone_core_get_default_proxy_config(LC);
    if (!pCfg) {
        LOG(@"not exist pCfg");
        return nil;
    }
    
    const char *domain = linphone_proxy_config_get_domain(pCfg);
    NSString *domainStr = [NSString stringWithCString:domain encoding:NSUTF8StringEncoding];
    if (!domainStr || [domainStr isEqualToString:@""]) {
        LOG(@"not exist domain");
        return nil;
    }
    
    // 地址字符串: sip:username@domain
    const char *identity = [NSString stringWithFormat:@"sip:%@@%@", to, domainStr].UTF8String;
    LinphoneChatRoom *chatRoom = linphone_core_get_chat_room_from_uri(LC, identity);
    if (!chatRoom) {
        LOG(@"not exist room");
        return nil;
    }
    
    LinphoneChatMessage *chatMessage = linphone_chat_room_create_message(chatRoom, message.UTF8String);
    if (!chatMessage) {
        return nil;
    }
    
    // 呼叫发送消息
    /*
    LinphoneInfoMessage *im = linphone_core_create_info_message(LC);
    LinphoneContent *content = linphone_core_create_content(LC);
    linphone_content_set_string_buffer(content, "aaaa");
    linphone_info_message_set_content(im, content);
    linphone_call_send_info_message(linphone_core_get_current_call(LC), im);
     */
    
    LinphoneChatMessageCbs *cbs = linphone_chat_message_get_callbacks(chatMessage);
    if (!cbs) {
        return nil;
    }
    // 设置回调
    linphone_chat_message_cbs_set_msg_state_changed(cbs, message_status);
    
    // 发送消息
    linphone_chat_room_send_chat_message(chatRoom, chatMessage);
    
    // 返回MessageID
    return [XQLPOCBridge getMessageIDWithChatMessage:chatMessage];
}

static void message_status(LinphoneChatMessage *msg, LinphoneChatMessageState state) {
    LOG(@"\n\n\n message_status text = %s status = %d \n\n\n", linphone_chat_message_get_text(msg), state);
    NSString *textStr = [XQLPOCBridge getMessageWithChatMessage:msg];
    
    const LinphoneAddress *toAddress = linphone_chat_message_get_to_address(msg);
    NSString *to = [XQLPOCBridge getUserNameWithAddress:toAddress];
    
    const LinphoneAddress *fromAddress = linphone_chat_message_get_from_address(msg);
    NSString *from = [XQLPOCBridge getUserNameWithAddress:fromAddress];
    
    NSString *messageID = [XQLPOCBridge getMessageIDWithChatMessage:msg];
    
    [[LdsdSipManager manager] messageStatus:state message:textStr messageID:messageID to:to from:from];
    // 获取消息时间
//    linphone_chat_message_get_time(<#const LinphoneChatMessage *message#>);
    // 获取聊天记录, nb数量
//    linphone_chat_room_get_history(<#LinphoneChatRoom *cr#>, <#int nb_message#>);
    // 获取某个范围聊天记录
//    linphone_chat_room_get_history_range(<#LinphoneChatRoom *cr#>, <#int begin#>, <#int end#>);
}

- (void)messageStatus:(LinphoneChatMessageState)status message:(NSString *)message messageID:(NSString *)messageID to:(NSString *)to from:(NSString *)from  {
    if ([self.delegate respondsToSelector:@selector(ldsdSendMessageWithFrom:to:status:message:messageID:)]) {
        [self.delegate ldsdSendMessageWithFrom:from to:to status:(LdsdSipChatMessageState)status message:message messageID:messageID];
    }
}

#pragma mark -- 呼叫
- (BOOL)ldsdCall:(NSString *)to {
    if (![self lcIsExist]) return NO;
    
    if (!to || [to isEqualToString:@""]) {
        LOG(@"param error");
        return NO;
    }
    
    LinphoneCall *call = linphone_core_get_current_call(LC);
    if (call) {
        LOG(@"Is on the phone");
        return NO;
    }
    
    LinphoneProxyConfig *pCfg = linphone_core_get_default_proxy_config(LC);
    if (!pCfg) {
        LOG(@"pCfg not exist");
        return NO;
    }
    
    // sip:username@domain
    const LinphoneAddress *address = linphone_proxy_config_get_identity_address(pCfg);
    NSString *domain = [NSString stringWithCString:linphone_address_get_domain(address) encoding:NSUTF8StringEncoding];
    const char *addr = [NSString stringWithFormat:@"sip:%@@%@", to, domain].UTF8String;
    
    LinphoneAddress *linphoneAddress = linphone_address_new(addr);
    
    if (!linphoneAddress) {
        LOG(@"address not exist");
        return NO;
    }
    
    /** 视频 */
    // 开始视频显示
    // linphone_core_enable_video_display(LC, TRUE);
    // 开启视频捕抓
    // linphone_core_enable_video_capture(LC, TRUE);
    // 开启视频预览
    // linphone_core_enable_video_preview(LC, TRUE);
    // 是否显示视频
    // linphone_core_show_video(LC, true);
    
    // 自动接收视频
    // const LinphoneVideoPolicy *videoPolicy = linphone_core_get_video_policy(LC);
    // LinphoneVideoPolicy *changeVPolicy = videoPolicy;
    // changeVPolicy->automatically_accept = true;
    // linphone_core_set_video_policy(LC, changeVPolicy);
    
    // 获取支持视频的大小
    // linphone_core_get_supported_video_sizes(LC)
    
    // 视频预览大小
    // linphone_core_set_preview_video_size(LC, ms_video_size_make(50, 50));
    
    // 会议是否启动视频(启动会议模式，就有点像QQ讨论组)
    // linphone_conference_params_enable_video(linphone_conference_params_new(LC), true);
    
//    linphone_core_payload_type_enabled(<#LinphoneCore *lc#>, <#const LinphonePayloadType *pt#>);
    
    return [[LinphoneManager instance] call:linphoneAddress];
}

#pragma mark -- 接听
- (BOOL)ldsdAnswer {
    if (![self lcIsExist]) return NO;
    
    LinphoneCall *call = linphone_core_get_current_call(LC);
    if (!call) {
        LOG(@"call not exist");
        return NO;
    }
    
    [[LinphoneManager instance] acceptCall:call evenWithVideo:LinphoneManager.instance.enableVideo];
    
    return YES;
}

#pragma mark -- 挂断
- (BOOL)ldsdHangUp {
    if (![self lcIsExist]) return NO;
    
    LinphoneCall *call = linphone_core_get_current_call(LC);
    if (!call) {
        LOG(@"call not exist");
        return NO;
    }
    
    // 挂断单个电话
    /*
     LinphoneErrorInfo *ei = linphone_error_info_new();
     linphone_error_info_set(ei, "tcp", LinphoneReasonDeclined, 0, "我就是挂断了", "警告信息");
     linphone_call_terminate_with_error_info(call, ei);
     */
    
    // 挂断所有call
    linphone_core_terminate_all_calls(LC);
    
    return YES;
}

#pragma mark -- 快照
- (BOOL)ldsdSnapShot:(NSString *)path {
    if (![self lcIsExist]) return NO;
    
    LinphoneCall *call = linphone_core_get_current_call(LC);
    if (!call) {
        LOG(@"call not exist");
        return NO;
    }
    
    int result = linphone_call_take_video_snapshot(call, path.UTF8String);
    
    if (result == 0) {
        return YES;
    }else {
        return NO;
    }
}

#pragma mark -- 设置视频的接发 暂时不用留接口
- (BOOL)setVideoWithDir:(LdsdSipMediaDirection)dir {
    if (![self lcIsExist]) return NO;
    
    LinphoneManager.instance.dir = (LinphoneMediaDirection)dir;
    
    LinphoneCall *call = linphone_core_get_current_call(LC);
    if (call) {
        const LinphoneCallParams *params = linphone_call_get_current_params(call);
        LinphoneCallParams *copyParams = linphone_call_params_copy(params);
        // 只接收视频
        linphone_call_params_set_video_direction(copyParams, (LinphoneMediaDirection)dir);
        
        linphone_call_update(call, params);
        linphone_call_params_unref(copyParams);
    }
    
    return YES;
}

#pragma mark -- 是否可用视频 暂时不用留接口
- (BOOL)setVideoEnable:(BOOL)enable {
    if (![self lcIsExist]) return NO;
    
    LinphoneManager.instance.enableVideo = enable;
    
    LinphoneCall *call = linphone_core_get_current_call(LC);
    if (call) {
        const LinphoneCallParams *params = linphone_call_get_current_params(call);
        LinphoneCallParams *copyParams = linphone_call_params_copy(params);
        
        linphone_call_params_enable_video(copyParams, enable);
        
        linphone_call_update(call, params);
        linphone_call_params_unref(copyParams);
    }
    
    return YES;
}

#pragma mark -- 设置麦克风
- (BOOL)ldsdMuteEnabled:(BOOL)mute {
    if (![self lcIsExist]) return NO;
    linphone_core_enable_mic(LC, !mute);
    return YES;
}

#pragma mark -- 设置本地摄像头 暂时不用暴露接口 
/** 本地摄像头
 *  @prama camera 1前置摄像头， 0后置摄像头
 */
- (BOOL)ldsdSetLocalCamera:(int)camera {
    if (![self lcIsExist]) return NO;
    
    char **camlist = (char **)linphone_core_get_video_devices(LC);
    // 是否存在摄像设备
    if (camlist) {
        for (char *cam = *camlist; *camlist != NULL; cam = *++camlist) {
            if (camera == 1 && strcmp(FRONT_CAM_NAME, cam) == 0) {
                linphone_core_set_video_device(LC, cam);
                return YES;
            }else if (camera == 0 && strcmp(BACK_CAM_NAME, cam)) {
                linphone_core_set_video_device(LC, cam);
                return YES;
            }
        }
        
    }else {
        LOG(@"No camera detected!");
        return NO;
    }
    
    return NO;
}

#pragma mark -- 远端视频window
- (BOOL)ldsdSetRemoteVideoView:(UIView *)view {
    if (![self lcIsExist]) return NO;
    if (view) {
        linphone_core_set_native_video_window_id(LC, (void*)((unsigned long)view));
    }
    
    /*
    // 获取params
    LdsdSipCall *call = LdsdSip_core_get_current_call(LC);
    
    LdsdSipCallParams *call_params = LdsdSip_core_create_call_params(LC,call);
    
    if (sender.selected) {
        LdsdSip_call_params_enable_video(call_params, FALSE);
        
    }else {
        LdsdSipCallAppData *callAppData = (__bridge LdsdSipCallAppData *)LdsdSip_call_get_user_pointer(call);
        callAppData->videoRequested = TRUE; // will be used later to notify user if video was not activated because of the LdsdSip core
        LdsdSip_call_params_enable_video(call_params, TRUE);
        
    }
    
    LdsdSip_call_update(call, call_params);
    LdsdSip_call_params_unref(call_params);
     */
    
    return YES;
}

#pragma mark -- 本地视频window 暂时不用暴露接口
- (BOOL)setLocalVideoView:(UIView *)view {
    if (![self lcIsExist]) return NO;
    if (view) {
        linphone_core_set_native_preview_window_id(LC, (void*)((unsigned long)view));
    }
    
    return YES;
}

#pragma mark -- 设置stun
- (BOOL)ldsdSetStun:(NSString *)stunServer {
    if (![self lcIsExist]) return NO;
    
    LinphoneNatPolicy *policy = linphone_core_get_nat_policy(LC);
    
    if (!stunServer || [stunServer isEqualToString:@""]) {
        // 穿透
        linphone_core_set_stun_server(LC, NULL);
        linphone_nat_policy_set_stun_server(policy, NULL);
        
    }else {
        // 穿透 "stunserver.org"
        linphone_core_set_stun_server(LC, stunServer.UTF8String);
        linphone_nat_policy_set_stun_server(policy, stunServer.UTF8String);
    }
    
    linphone_core_set_nat_policy(LC, policy);
    
    return YES;
}

- (BOOL)ldsdStunEnabled:(BOOL)enabled {
    if (![self lcIsExist]) return NO;
    LinphoneNatPolicy *policy = linphone_core_create_nat_policy(LC);
    // 开启stun
    linphone_nat_policy_enable_stun(policy, enabled);
    linphone_core_set_nat_policy(LC, policy);
    
    return YES;
}

#pragma mark -- 设置ice

- (BOOL)ldsdICEEnabled:(BOOL)enabled {
    if (![self lcIsExist]) return NO;
    // 防火墙
    // 获取policy
    LinphoneNatPolicy *policy = linphone_core_create_nat_policy(LC);
    // 开启ICE
    linphone_nat_policy_enable_ice(policy, enabled);
    // 设置policy
    linphone_core_set_nat_policy(LC, policy);
    return YES;
}

#pragma mark -- timer

- (void)createTimer {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(checkInCall) userInfo:nil repeats:YES];
}

- (void)checkInCall {
    BOOL isIn = linphone_core_in_call(LC);
    BOOL isIncoming = linphone_core_is_incoming_invite_pending(LC);
    
    if (!isIn && !isIncoming) {
        [self.declineSipArr removeAllObjects];
    }
}

#pragma mark -- linphone通知

- (void)noticeGlobal:(NSNotification *)notification {
    // 暂时不用
    /*
    NSDictionary *dic = notification.userInfo;
    NSNumber *state = dic[@"state"];
    if ([self.delegate respondsToSelector:@selector(ldsdSipInitWithStatus:)]) {
        [self.delegate ldsdSipInitWithStatus:state.integerValue];
    }
     */
}

- (void)noticeRegistration:(NSNotification *)notification {
    NSDictionary *dic = notification.userInfo;
    NSNumber *state = dic[@"state"];
    NSString *username = dic[@"username"];
    NSString *message = dic[@"message"];
    //NSString *domain = dic[@"domain"];
    
    if ([self.delegate respondsToSelector:@selector(ldsdRegistrationStateUpdate:username:message:)]) {
        [self.delegate ldsdRegistrationStateUpdate:state.integerValue username:username message:message];
    }
}

- (void)noticeCallChange:(NSNotification *)notification {
    NSDictionary *dic = notification.userInfo;
    NSNumber *state = dic[@"state"];
    NSString *message = dic[@"message"];
    NSString *fromUserName = dic[@"from"];
    NSString *fromDisplayName  = dic[@"fromDisplayName"];
    NSString *toUserName = dic[@"to"];
    //NSString *toDisplayName  = dic[@"toDisplayName"];
    
    // 来电
    if (state.integerValue == LdsdSipCallIncomingReceived) {
        // 获取当前通话列表
        const bctbx_list_t *callList = linphone_core_get_calls(LC);
        // 有两个通话
        if (callList->next) {
            // 拒绝当前这个通话
            LinphoneCall *call = [dic[@"call"] pointerValue];
            if (call) {
                linphone_call_decline(call, LinphoneReasonBusy);
            }
            
            [self.declineSipArr addObject:fromUserName];
            return;
        }
    }
    
    // 存在挂断数组, 并且是通话结束或者通话释放
    if (self.declineSipArr.count != 0 && (state.integerValue == LdsdSipCallEnd || state.integerValue == LdsdSipCallReleased)) {
        // 包含来电sip, 不回调
        if ([self.declineSipArr containsObject:fromUserName]) {
            if (state.integerValue == LdsdSipCallReleased) {
                [self.declineSipArr removeObject:fromUserName];
            }
            return;
        }
        
    }
    
    if ([self.delegate respondsToSelector:@selector(ldsdCallStateChangeWithFrom:to:fromDisplayName:status:message:)]) {
        [self.delegate ldsdCallStateChangeWithFrom:fromUserName to:toUserName fromDisplayName:fromDisplayName status:(LdsdSipCall)state.integerValue message:message];
    }
}

- (void)noticeCallUpdate:(NSNotification *)notification {
    NSDictionary *dic = notification.userInfo;
    NSNumber *state = dic[@"state"];
    NSString *fromUserName = dic[@"from"];
    NSString *fromDisplayName  = dic[@"fromDisplayName"];
    NSString *toUserName = dic[@"to"];
    
    if ([self.delegate respondsToSelector:@selector(ldsdCallStateUpdateWithFrom:to:fromDisplayName:type:)]) {
        [self.delegate ldsdCallStateUpdateWithFrom:fromUserName to:toUserName fromDisplayName:fromDisplayName type:(LdsdSipStreamType)state.integerValue];
    }
}

- (void)noticeMessageReceived:(NSNotification *)notification {
    NSDictionary *dic = notification.userInfo;
    NSString *from = dic[@"from"];
    NSString *to = dic[@"to"];
    NSString *text = dic[@"text"];
    NSString *fromDisplayName = dic[@"fromDisplayName"];
    NSString *messageID = dic[@"messageID"];
    if ([self.delegate respondsToSelector:@selector(ldsdReceiveMessageWithFrom:to:fromDisplayName:message:messageID:)]) {
        [self.delegate ldsdReceiveMessageWithFrom:from to:to fromDisplayName:fromDisplayName message:text messageID:messageID];
    }
}

#pragma mark -- get

- (NSMutableArray *)declineSipArr {
    if (!_declineSipArr) {
        _declineSipArr = [NSMutableArray array];
    }
    return _declineSipArr;
}

@end



















