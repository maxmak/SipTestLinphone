//
//  LdsdSipHeader.h
//  XQLdsdSip
//
//  Created by ladystyle100 on 2017/8/5.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#ifndef LdsdSipHeader_h
#define LdsdSipHeader_h

typedef NS_ENUM(NSUInteger, LdsdSipGlobal) {
    LdsdSipGlobalOff,
    LdsdSipGlobalStartup,
    LdsdSipGlobalOn,
    LdsdSipGlobalShutdown,
    LdsdSipGlobalConfiguring
};

typedef NS_ENUM(NSInteger, LdsdSipRegistration) {
    LdsdSipRegistrationNone, /**< Initial state for registrations */
    LdsdSipRegistrationProgress, /**< Registration is in progress */
    LdsdSipRegistrationOk,	/**< Registration is successful */
    LdsdSipRegistrationCleared, /**< Unregistration succeeded */
    LdsdSipRegistrationFailed	/**< Registration failed */
};

typedef NS_ENUM(NSInteger, LdsdSipCall) {
    LdsdSipCallIdle, /**< Initial call state */
    LdsdSipCallIncomingReceived, /**< This is a new incoming call */
    LdsdSipCallOutgoingInit, /**< An outgoing call is started */
    LdsdSipCallOutgoingProgress, /**< An outgoing call is in progress */
    LdsdSipCallOutgoingRinging, /**< An outgoing call is ringing at remote end */
    LdsdSipCallOutgoingEarlyMedia, /**< An outgoing call is proposed early media */
    LdsdSipCallConnected, /**< Connected, the call is answered */
    LdsdSipCallStreamsRunning, /**< The media streams are established and running */
    LdsdSipCallPausing, /**< The call is pausing at the initiative of local end */
    LdsdSipCallPaused, /**< The call is paused, remote end has accepted the pause */
    LdsdSipCallResuming, /**< The call is being resumed by local end */
    LdsdSipCallRefered, /**< The call is being transfered to another party, resulting in a new outgoing call to follow immediately */
    LdsdSipCallError, /**< The call encountered an error */
    LdsdSipCallEnd, /**< The call ended normally */
    LdsdSipCallPausedByRemote, /**< The call is paused by remote end */
    LdsdSipCallUpdatedByRemote, /**< The call's parameters change is requested by remote end, used for example when video is added by remote */
    LdsdSipCallIncomingEarlyMedia, /**< We are proposing early media to an incoming call */
    LdsdSipCallUpdating, /**< A call update has been initiated by us */
    LdsdSipCallReleased, /**< The call object is no more retained by the core */
    LdsdSipCallEarlyUpdatedByRemote, /**< The call is updated by remote while not yet answered (early dialog SIP UPDATE received) */
    LdsdSipCallEarlyUpdating /**< We are updating the call while not yet answered (early dialog SIP UPDATE sent) */
};

typedef NS_ENUM(NSInteger, LdsdSipChatMessageState) {
    LdsdSipChatMessageStateIdle, /**< Initial state */
    LdsdSipChatMessageStateInProgress, /**< Delivery in progress */
    LdsdSipChatMessageStateDelivered, /**< Message successfully delivered and acknowledged by server */
    LdsdSipChatMessageStateNotDelivered, /**< Message was not delivered */
    LdsdSipChatMessageStateFileTransferError, /**< Message was received(and acknowledged) but cannot get file from server */
    LdsdSipChatMessageStateFileTransferDone, /**< File transfer has been completed successfully */
    LdsdSipChatMessageStateDeliveredToUser, /**< Message successfully delivered and acknowledged to destination */
    LdsdSipChatMessageStateDisplayed /**< Message displayed to the remote user */
};

typedef NS_ENUM(NSInteger, LdsdSipMediaDirection) {
    LdsdSipMediaDirectionInvalid = -1,
    LdsdSipMediaDirectionInactive, /** No active media not supported yet*/
    LdsdSipMediaDirectionSendOnly, /** Send only mode*/
    LdsdSipMediaDirectionRecvOnly, /** recv only mode*/
    LdsdSipMediaDirectionSendRecv, /** send receive*/
};

typedef NS_ENUM(NSInteger, LdsdSipStreamType)  {
    LdsdSipStreamTypeAudio,
    LdsdSipStreamTypeVideo,
    LdsdSipStreamTypeText,
    LdsdSipStreamTypeUnknown /* WARNING: Make sure this value remains the last one in the list */
};


#endif /* LdsdSipHeader_h */










