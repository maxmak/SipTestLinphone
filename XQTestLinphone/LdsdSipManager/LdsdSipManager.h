//
//  LdsdSipManager.h
//  XQLinphone
//
//  Created by ladystyle100 on 2017/8/5.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LdsdSipHeader.h"

@protocol LdsdSipManagerDelegate <NSObject>

@optional

/** 登录/登出
 *  @param status LdsdSipRegistrationProgress正在登录/正在登出
 *                LdsdSipRegistrationOk登录成功
 *                LdsdSipRegistrationCleared登出成功
 *                LdsdSipRegistrationFailed登录/登出失败
 *  @param username 登录/登出sip
 *  @param message 登录/登出消息
 */
- (void)ldsdRegistrationStateUpdate:(LdsdSipRegistration)status
                           username:(NSString *)username
                            message:(NSString *)message;

/** 通话状态改变
 *  @param from 主呼人
 *  @param to 被呼人
 *  @param fromDisplayName 主呼人昵称
 *  @param status 状态码
 *  @param message 通话消息
 */
- (void)ldsdCallStateChangeWithFrom:(NSString *)from
                                 to:(NSString *)to
                    fromDisplayName:(NSString *)fromDisplayName
                             status:(LdsdSipCall)status
                            message:(NSString *)message;

/** 通话更新
 *  @param from 主呼人
 *  @param to 被呼人
 *  @param fromDisplayName 主呼人昵称
 *  @param type 更新的流类型
 */
- (void)ldsdCallStateUpdateWithFrom:(NSString *)from
                                 to:(NSString *)to
                    fromDisplayName:(NSString *)fromDisplayName
                               type:(LdsdSipStreamType)type;

/** 接收到消息
 *  @param from 主发消息人
 *  @param to 接收消息人
 *  @param fromDisplayName 发消息人昵称
 *  @param message 消息
 *  @param messageID 消息标识符
 */
- (void)ldsdReceiveMessageWithFrom:(NSString *)from
                                to:(NSString *)to
                   fromDisplayName:(NSString *)fromDisplayName
                           message:(NSString *)message
                         messageID:(NSString *)messageID;

/** 发出去消息状态
 *  @param from 主发消息人
 *  @param to 接收消息人
 *  @param status LdsdSipChatMessageStateInProgress正在发送 
 *                LdsdSipChatMessageStateDelivered发送成功
 *                LdsdSipChatMessageStateNotDelivered发送失败
 *  @param message 消息
 *  @param messageID 消息标识符
 */
- (void)ldsdSendMessageWithFrom:(NSString *)from
                             to:(NSString *)to
                         status:(LdsdSipChatMessageState)status
                        message:(NSString *)message
                      messageID:(NSString *)messageID;

@end

@interface LdsdSipManager : NSObject

+ (instancetype)manager;

/** 初始化sip */
- (void)initSipServer;

/** 是否开启log
 *  @param enable YES开启(默认开启)
 */
- (void)enableLogs:(BOOL)enable;

@property (nonatomic, weak) id <LdsdSipManagerDelegate> delegate;

/** 登录
 *  @param userName  sip
 *  @param password  密码
 *  @param displayName  昵称
 *  @param domain    域名或IP
 *  @param port      端口
 *  @param transport 连接方式
 *  return YES参数没有问题, 正常登录(注意:这并不是登录成功, 只是正常登录)
 */
- (BOOL)ldsdLogin:(NSString *)userName
         password:(NSString *)password
      displayName:(NSString *)displayName
           domain:(NSString *)domain
             port:(NSString *)port
    withTransport:(NSString *)transport;

/** 登出某一个账号
 *  @param userName sip
 *  @param domain 域名或IP
 *  return YES参数没有问题, 正常退出
 */
- (BOOL)ldsdLogout:(NSString *)userName
            domain:(NSString *)domain;

/** 登出所有 */
- (BOOL)ldsdLogoutAll;

/** 发送消息
 *  @param to 对方sip
 *  @param message 发送的消息
 *  return messageID, nil或@""为发送失败
 */
- (NSString *)ldsdSendMessage:(NSString *)to message:(NSString *)message;

/** 呼叫
 *  @param userName 对方sip
 *  return YES参数没有问题, 正常呼叫
 */
- (BOOL)ldsdCall:(NSString *)userName;

/** 接听当前通话 */
- (BOOL)ldsdAnswer;

/** 挂断当前通话 */
- (BOOL)ldsdHangUp;

/** 快照
 *  @param path 保存路径
 *  return YES保存成功
 */
- (BOOL)ldsdSnapShot:(NSString *)path;

/** 麦克风
 *  @pamra mute YES静音
 *  return YES参数没有问题, 正常设置
 */
- (BOOL)ldsdMuteEnabled:(BOOL)mute;

/** 远端视频window
 *  @param view 显示对方视频的view
 *  return YES参数没有问题, 正常设置
 */
- (BOOL)ldsdSetRemoteVideoView:(UIView *)view;

/** 设置stun服务器
 *  @param stunServer stun服务器
 *  return YES参数没有问题, 正常设置
 */
- (BOOL)ldsdSetStun:(NSString *)stunServer;

/** 设置是否开启stun
 *  @param enabled YES开启stun
 *  return YES参数没有问题, 正常设置
 */
- (BOOL)ldsdStunEnabled:(BOOL)enabled;

/** 设置是否开启ICE
 *  @param enabled YES开启
 *  return YES参数没有问题, 正常设置
 */
- (BOOL)ldsdICEEnabled:(BOOL)enabled;

@end











